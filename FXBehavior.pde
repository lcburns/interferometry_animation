class FXBehavior {
  boolean active;
  int currentMode, currentBehavior, currentIndex;
  float launchTime, expireTime, duration, percentageComplete;
  float halfWidth;
  color[][] palettes; // first dimension correlates with four palette types, second is minimum length of four input palettes
  color black = color(0);
  color trspblack = color(0, 1);
  Pattern maskWidth, maskOrigin, maskTraversal, paletteIndex;

  FXBehavior(color[] palette1, color[] palette2, color[] palette3, color[] palette4) {
    int partialLengthTest = min(palette1.length, palette2.length, palette3.length);
    int paletteLength = min(partialLengthTest, palette4.length);
    palettes = new color[4][paletteLength];
    for (int i = 0; i < paletteLength; i++) {
      palettes[0][i] = palette1[i];
      palettes[1][i] = palette2[i];
      palettes[2][i] = palette3[i];
      palettes[3][i] = palette4[i];
    }
    maskWidth = new Pattern(0, width * 0.25, 0); // was width * 0.1, width * 0.5
    maskOrigin = new Pattern(0, 3.99, 0); 
    maskTraversal = new Pattern(width * -0.08, width * 0.08, 0); // was width * +/- 0.2
    paletteIndex = new Pattern(0, paletteLength - 0.01, 0);
    halfWidth = width * 0.5;
  }

  void encoderMessage(int eNumber, int eValue) {
    switch(eNumber) {
    case 2: // fxCount
      paletteIndex.update(eValue);
      break;
    case 5: // layer 1 values
      maskWidth.update(eValue);
      break;
    case 9: // layer 2 values
      maskOrigin.update(eValue);
      break;
    case 13: // layer 3 values
      maskTraversal.update(eValue);
      break;
    default: // no-op on other encoders for now...
    }
  }

  void setBehavior(int mode, int FXID, int patternIndex, float d) {
    if (!active) {
      active = true;
      currentMode = mode;
      currentBehavior = FXID; // not treating 0-4 (left) and 5-9 (right) as equivalent - would be FXID % 5;
      currentIndex = patternIndex;
      launchTime = millis();
      duration = d;
      expireTime = launchTime + duration;
    }
  }

  void update() {
    if (active) {
      float currentTime = millis();
      if (currentTime > expireTime) {
        active = false;
      } else {
        percentageComplete = (currentTime - launchTime) / duration;
      }
    }
  }

  void display() {
    if (active) {
      rectMode(CORNERS);
      switch(currentBehavior) {
      case 0: // no background; left side split between fading transparent mask and black mask; right side masked
        noStroke();
        switch(int(maskOrigin.getValue(currentIndex))) {
        case 0:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(0, 0, maskWidth.getValue(currentIndex), height);  
          fill(black);
          rect(maskWidth.getValue(currentIndex), 0, halfWidth, height);
          break;        
        case 1:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(maskWidth.getValue(currentIndex), 0, halfWidth, height);
          fill(black);
          rect(0, 0, maskWidth.getValue(currentIndex), height);          
          break;
        case 2:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(0, 0, halfWidth, maskWidth.getValue(currentIndex));
          fill(black);
          rect(0, maskWidth.getValue(currentIndex), halfWidth, height);
          break;
        default:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(0, maskWidth.getValue(currentIndex), halfWidth, height);
          fill(black);
          rect(0, 0, halfWidth, maskWidth.getValue(currentIndex));
        }
        fill(black);
        rect(halfWidth, 0, width, height);
        break;
      case 1: // no background; left side split between moving/fading transparent mask and black mask; right side masked
        noStroke();
        switch(int(maskOrigin.getValue(currentIndex))) {
        case 0:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(0, 0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);
          fill(black);
          rect(maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, halfWidth, height);
          break;
        case 1:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, halfWidth, height);
          fill(black);
          rect(0, 0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);          
          break;
        case 2:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(0, 0, halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
          fill(black);
          rect(0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), halfWidth, height);
          break;
        default:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), halfWidth, height);
          fill(black);
          rect(0, 0, halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
        }
        fill(black);
        rect(halfWidth, 0, width, height);
        break;
      case 2: // no background; left side split between moving/transparent mask and black mask; right side masked
        noStroke();
        switch(int(maskOrigin.getValue(currentIndex))) {
        case 0:
          fill(trspblack);
          rect(0, 0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);
          fill(black);
          rect(maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, halfWidth, height);
          break;
        case 1:
          fill(trspblack);
          rect(maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, halfWidth, height);
          fill(black);
          rect(0, 0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);          
          break;
        case 2:
          fill(trspblack);
          rect(0, 0, halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
          fill(black);
          rect(0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), halfWidth, height);
          break;
        default:
          fill(trspblack);
          rect(0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), halfWidth, height);
          fill(black);
          rect(0, 0, halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
        }
        fill(black);
        rect(halfWidth, 0, width, height);
        break;
      case 3: // no background; left side split between moving/transparent mask and no mask; right side masked
        noStroke();
        fill(trspblack);
        switch(int(maskOrigin.getValue(currentIndex))) {
        case 0:
          rect(0, 0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);
          break;
        case 1:
          rect(maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, halfWidth, height);
          break;
        case 2:
          rect(0, 0, halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
          break;
        default:
          rect(0, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), halfWidth, height);
        }
        fill(black);
        rect(halfWidth, 0, width, height);
        break;
      case 4: // no background; left side unmasked, right side  masked
        noStroke();
        fill(black);
        rect(halfWidth, 0, width, height);
        break;
      case 5: // no background; right side split between fading transparent mask and black mask; left side masked
        noStroke();
        switch(int(maskOrigin.getValue(currentIndex))) {
        case 0:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(halfWidth, 0, halfWidth + maskWidth.getValue(currentIndex), height);           
          fill(black);
          rect(halfWidth + maskWidth.getValue(currentIndex), 0, width, height);
          break;          
        case 1:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(halfWidth + maskWidth.getValue(currentIndex), 0, width, height);
          fill(black);
          rect(halfWidth, 0, halfWidth + maskWidth.getValue(currentIndex), height);          
          break;
        case 2:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(halfWidth, 0, width, maskWidth.getValue(currentIndex));
          fill(black);
          rect(halfWidth, maskWidth.getValue(currentIndex), width, height);
          break;
        default:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(halfWidth, maskWidth.getValue(currentIndex), width, height);
          fill(black);
          rect(halfWidth, 0, width, maskWidth.getValue(currentIndex));
        }
        fill(black);
        rect(0, 0, halfWidth, height);
        break;
      case 6: // no background; right side split between moving/fading transparent mask and black mask; left side masked
        noStroke();
        switch(int(maskOrigin.getValue(currentIndex))) {
        case 0:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(halfWidth, 0, halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);
          fill(black);
          rect(halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, width, height);
          break;
        case 1:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, width, height);
          fill(black);
          rect(halfWidth, 0, halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);          
          break;
        case 2:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(halfWidth, 0, width, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
          fill(black);
          rect(halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), width, height);
          break;
        default:
          fill(lerpColor(trspblack, black, pow(percentageComplete, 2)));
          rect(halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), width, height);
          fill(black);
          rect(halfWidth, 0, width, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
        }
        fill(black);
        rect(0, 0, halfWidth, height);
        break;
      case 7: // no background; right side split between moving/transparent mask and black mask; left side masked
        noStroke();
        switch(int(maskOrigin.getValue(currentIndex))) {
        case 0:
          fill(trspblack);
          rect(halfWidth, 0, halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);
          fill(black);
          rect(halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, width, height);
          break;
        case 1:
          fill(trspblack);
          rect(halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, width, height);
          fill(black);
          rect(halfWidth, 0, halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);          
          break;
        case 2:
          fill(trspblack);
          rect(halfWidth, 0, width, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
          fill(black);
          rect(halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), width, height);
          break;
        default:
          fill(trspblack);
          rect(halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), width, height);
          fill(black);
          rect(halfWidth, 0, width, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
        }
        fill(black);
        rect(0, 0, halfWidth, height);
        break;
      case 8: // no background; right side split between moving/transparent mask and no mask; left side masked
        noStroke();
        fill(trspblack);
        switch(int(maskOrigin.getValue(currentIndex))) {
        case 0:
          rect(halfWidth, 0, halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), height);
          break;
        case 1:
          rect(halfWidth + maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), 0, width, height);
          break;
        case 2:
          rect(halfWidth, 0, width, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete));
          break;
        default:
          rect(halfWidth, maskWidth.getValue(currentIndex) - (maskTraversal.getValue(currentIndex) * percentageComplete), width, height);
        }
        fill(black);
        rect(0, 0, halfWidth, height);
        break;
      case 9: // no background; right side unmasked, left side  masked
        noStroke();
        fill(black);
        rect(0, 0, halfWidth, height);
        break;
      }
    } else {
      background(0);
    }
  }
}
