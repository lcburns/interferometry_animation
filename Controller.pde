class Controller {
  int maxLength = 20;
  boolean[] controlArray; // false = aValues, true = bValues

  Controller(int init) {
    controlArray = new boolean[maxLength];
    this.update(init);
  }

  boolean getValue(int index) {
    // test for boundaries?
    boolean returnValue = controlArray[index];
    return returnValue;
  }

  boolean[] getArray() {
    return controlArray;
  }

  void update(int controllerInput) {
    int updateInput = controllerInput % 32;
    switch(updateInput) {
    case 1: 
    case 30:
      randomizeController(false, 1);
      break;
    case 2: 
    case 29:
      randomizeController(false, 2);
      break;
    case 3: 
    case 28:
      randomizeController(false, 3);
      break;
    case 4: 
    case 27:
      randomizeController(false, 4);
      break;
    case 5: 
    case 26:
      randomizeController(false, 6);
      break;
    case 6: 
    case 25:
      randomizeController(false, 8);
      break;
    case 7: 
    case 24:
      randomizeController(false, 10);
      break;
    case 8: 
    case 23:
      randomizeController(true, 10);
      break;
    case 9: 
    case 22:
      randomizeController(true, 8);
      break;
    case 10: 
    case 21:
      randomizeController(true, 6);
      break;
    case 11: 
    case 20:
      randomizeController(true, 4);
      break;
    case 12: 
    case 19:
      randomizeController(true, 3);
      break;
    case 13: 
    case 18:
      randomizeController(true, 2);
      break;
    case 14: 
    case 17:
      randomizeController(true, 1);
      break;
    case 15:
    case 16:
      setController(true);
      break;
    default: // case 0, 31
      setController(false);
    }
  }

  void setController(boolean setValue) {
    for (int i = 0; i < maxLength; i++) {
      controlArray[i] = setValue;
    }
  }

  void randomizeController(boolean baseValue, int pokeInstances) {
    for (int i = 0; i < maxLength; i++) {
      controlArray[i] = baseValue;
    }
    for (int i = 0; i < pokeInstances; i++) {
      controlArray[int(random(maxLength))] = !baseValue;
    }
  }
}
