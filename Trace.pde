class Trace {
  boolean patternGlobal, gainActive;
  int patternLength = 20;
  int maxSegmentCount, segmentCount, segmentIndex, colorIndex1, colorIndex2;
  float sineTheta, compTheta1, compTheta2, baseline, segmentX, fadeStart;
  color fadeColor;
  boolean[] active;
  float[] sineX1, sineY1, sineLength, sineAngle, sineWeight, sineColorValue;
  float[] compX1, compY1, compLength, compAngle, compWeight, compColorValue;  
  float[] lineX1, lineY1, lineX2, lineY2, lineWeight, lineColorValue, fadeValue;
  color[] colorPalette1, colorPalette2;
  Pattern sineXVelocity, sineYVelocity, sineLengthDelta, sineAngleDelta, sineWeightStart, sineWeightDelta, sineColorDelta; 
  Pattern compXVelocity, compYVelocity, compLengthDelta, compAngleDelta, compWeightStart, compWeightDelta, compColorDelta; 
  Pattern lineXVelocity, lineYVelocity, lineX2Delta, lineY2Delta, lineWeightStart, lineWeightDelta, lineColorDelta, fadeDelta;
  Pattern sineDelta, sineAmp, lineAmp, segmentXDelta, colorStart, segments;
  Pattern compDelta1, compDelta2, compAmp1, compAmp2;

  Trace(int sc, float b, float fadeMin, float fadeMax, float heightMax, color[] cp1, color[] cp2, color fc, boolean pg, float weightMin, float weightMax, float velocity) {
    maxSegmentCount = max(sc, 12);
    segmentCount = maxSegmentCount;
    baseline = b;
    colorPalette1 = cp1;
    colorPalette2 = cp2;
    fadeColor = fc;
    patternGlobal = pg;
    active = new boolean[maxSegmentCount];
    sineX1 = new float[maxSegmentCount];
    sineY1 = new float[maxSegmentCount];  
    sineLength = new float[maxSegmentCount];
    sineAngle = new float[maxSegmentCount];  
    sineWeight = new float[maxSegmentCount];
    sineColorValue = new float[maxSegmentCount];
    compX1 = new float[maxSegmentCount];
    compY1 = new float[maxSegmentCount];  
    compLength = new float[maxSegmentCount];
    compAngle = new float[maxSegmentCount];  
    compWeight = new float[maxSegmentCount];
    compColorValue = new float[maxSegmentCount];
    lineX1 = new float[maxSegmentCount];
    lineY1 = new float[maxSegmentCount];  
    lineX2 = new float[maxSegmentCount];
    lineY2 = new float[maxSegmentCount];
    lineWeight = new float[maxSegmentCount];
    lineColorValue = new float[maxSegmentCount];
    fadeValue = new float[maxSegmentCount];
    sineXVelocity = new Pattern(velocity * -1, velocity, 0); // was 0.25
    sineYVelocity = new Pattern(velocity * -1, velocity, 0);
    compXVelocity = new Pattern(velocity * -1, velocity, 0); // was 0.25
    compYVelocity = new Pattern(velocity * -1, velocity, 0); 
    lineXVelocity = new Pattern(velocity * -0.5, velocity * 0.5, 0); // was 0.25
    lineYVelocity = new Pattern(velocity * -0.5, velocity * 0.5, 0);
    sineLengthDelta = new Pattern(-1, 1, 0);
    sineAngleDelta = new Pattern(-0.006, 0.006, 0); 
    sineWeightStart = new Pattern(weightMin, weightMax, 0); // was 15
    sineWeightDelta = new Pattern(0.0025, 0.075, 0);
    sineColorDelta = new Pattern(fadeMin * TWO_PI, fadeMax * TWO_PI * 2, 0);
    compLengthDelta = new Pattern(-1, 1, 0);
    compAngleDelta = new Pattern(-0.006, 0.006, 0); 
    compWeightStart = new Pattern(weightMin, weightMax, 0); // was 12
    compWeightDelta = new Pattern(0.0025, 0.075, 0);
    compColorDelta = new Pattern(fadeMin * TWO_PI, fadeMax * TWO_PI * 2, 0); 
    lineX2Delta = new Pattern(-1, 1, 0);
    lineY2Delta = new Pattern(-1.5, 1.5, 0);
    lineWeightStart = new Pattern(weightMin, weightMax, 0); // was 15
    lineWeightDelta = new Pattern(0.0025, 0.075, 0);
    lineColorDelta = new Pattern(fadeMin * TWO_PI, fadeMax * TWO_PI * 2, 0);
    fadeDelta = new Pattern(fadeMin, fadeMax, 0);
    sineAmp = new Pattern(height * 0.04, heightMax, 0);
    compAmp1 = new Pattern(height * 0.08, heightMax * 0.3, 0);
    compAmp2 = new Pattern(height * 0.12, heightMax * 0.3, 0);
    lineAmp = new Pattern(height * 0.004, height * 0.08, 0);
    if (patternGlobal) {
      sineDelta = new Pattern(0.01, 0.4, 0);
      compDelta1 = new Pattern(0.04, 0.5, 0);
      compDelta2 = new Pattern(0.16, 0.6, 0);
      //segmentXDelta = new Pattern(width / 48, width / 12, 0);
    } else {
      sineDelta = new Pattern(0.01, 0.125, 0);
      compDelta1 = new Pattern(0.04, 0.33, 0);
      compDelta2 = new Pattern(0.16, 0.4, 0);      
      //segmentXDelta = new Pattern(width / 96, width / 12, 0);
    }
    segmentXDelta = new Pattern(width / (segmentCount * 1.5), width / (segmentCount * 0.125), 0);
    colorStart = new Pattern(0, 1, 0);
    fadeStart = 0;
    segments = new Pattern(10, maxSegmentCount, 0);
    gainActive = true;
  }

  void patternMessage(int patternIndex) { 
    if (gainActive) {
      segmentCount = int(segments.getValue(patternIndex));
      if (patternGlobal) {
        float xDelta = segmentXDelta.getValue(patternIndex);
        float xPosition = (width - (segmentCount * xDelta)) * 0.5;
        float sAmp = sineAmp.getValue(patternIndex);
        float cAmp1 = compAmp1.getValue(patternIndex);
        float cAmp2 = compAmp2.getValue(patternIndex);
        float lAmp = lineAmp.getValue(patternIndex);
        float nlAmp = 0 - lAmp;
        float currentDelta = pow(sineDelta.getValue(patternIndex), 2); // reducing delta...
        float cDelta1 = pow(compDelta1.getValue(patternIndex), 2);
        float cDelta2 = pow(compDelta2.getValue(patternIndex), 2);
        sineTheta = 0;
        compTheta1 = 0;
        compTheta2 = 0;
        float lineY2temp = random(nlAmp, lAmp) + baseline; // initial position
        for (int i = 0; i < segmentCount; i++) {
          active[i] = true;
          float nextTheta = sineTheta + currentDelta;
          sineX1[i] = xPosition;
          compX1[i] = xPosition;
          lineX1[i] = xPosition;
          sineY1[i] = (sin(sineTheta) * sAmp) + baseline;
          compY1[i] = (sin(compTheta1) * cAmp1) + (sin(compTheta2) * cAmp2) + baseline;
          //lineY1[i] = random(nlAmp, lAmp) + baseline;
          lineY1[i] = lineY2temp;
          float x2 = sineX1[i] + xDelta;
          float sineY2 = (sin(nextTheta) * sAmp) + baseline;
          float compY2 = (sin(compTheta1 + cDelta1) * cAmp1) + (sin(compTheta2 + cDelta2) * cAmp2) + baseline;
          lineX2[i] = xPosition + xDelta;
          lineY2temp = random(nlAmp, lAmp) + baseline;
          lineY2[i] = lineY2temp;
          sineLength[i] = dist(sineX1[i], sineY1[i], x2, sineY2);
          sineAngle[i] = asin((sineY2 - sineY1[i]) / sineLength[i]);
          sineWeight[i] = sineWeightStart.getValue(patternIndex);
          compLength[i] = dist(compX1[i], compY1[i], x2, compY2);
          compAngle[i] = asin((compY2 - compY1[i]) / compLength[i]);
          compWeight[i] = compWeightStart.getValue(patternIndex);
          float colorInit = colorStart.getValue(patternIndex);
          sineColorValue[i] = colorInit;
          compColorValue[i] = colorInit;
          lineWeight[i] = lineWeightStart.getValue(patternIndex);
          lineColorValue[i] = colorInit;
          fadeValue[i] = fadeStart;
          xPosition += xDelta;
          sineTheta = nextTheta;
          compTheta1 += cDelta1;
          compTheta2 += cDelta2;
        }
      } else {
        // per-segment updates
        active[segmentIndex] = true;
        float nextTheta = sineTheta + sineDelta.getValue(patternIndex);
        float nextC1 = compTheta1 +  compDelta1.getValue(patternIndex);
        float nextC2 = compTheta2 +  compDelta2.getValue(patternIndex);
        sineX1[segmentIndex] = segmentX;
        compX1[segmentIndex] = segmentX;
        lineX1[segmentIndex] = segmentX;
        sineY1[segmentIndex] = (sin(sineTheta) * sineAmp.getValue(patternIndex)) + baseline;
        compY1[segmentIndex] = (sin(compTheta1) * compAmp1.getValue(patternIndex)) + (sin(compTheta2) * compAmp2.getValue(patternIndex)) + baseline;
        float lineAmpValue = lineAmp.getValue(patternIndex);
        //lineY1[segmentIndex] = random(0 - lineAmpValue, lineAmpValue) + baseline;
        if (segmentIndex == 0) {
          int refidx = segmentCount - 1;
          lineY1[segmentIndex] = lineY2[refidx];
        } else {
          int refidx = segmentIndex - 1;
          lineY1[segmentIndex] = lineY2[refidx];
        }
        segmentX += segmentXDelta.getValue(patternIndex);
        float x2 = segmentX;
        float sineY2 = (sin(nextTheta) * sineAmp.getValue(patternIndex)) + baseline;
        float compY2 = (sin(nextC1) * compAmp1.getValue(patternIndex)) + (sin(nextC2) * compAmp2.getValue(patternIndex)) + baseline;
        lineX2[segmentIndex] = x2;
        lineY2[segmentIndex] = random(0 - lineAmpValue, lineAmpValue) + baseline;
        sineLength[segmentIndex] = dist(sineX1[segmentIndex], sineY1[segmentIndex], x2, sineY2);
        sineAngle[segmentIndex] = asin((sineY2 - sineY1[segmentIndex]) / sineLength[segmentIndex]);
        sineWeight[segmentIndex] = sineWeightStart.getValue(segmentIndex % 20);
        sineColorValue[segmentIndex] = 0;
        compLength[segmentIndex] = dist(compX1[segmentIndex], compY1[segmentIndex], x2, compY2);
        compAngle[segmentIndex] = asin((compY2 - compY1[segmentIndex]) / compLength[segmentIndex]);
        compWeight[segmentIndex] = compWeightStart.getValue(segmentIndex % 20);
        compColorValue[segmentIndex] = 0;
        lineWeight[segmentIndex] = lineWeightStart.getValue(segmentIndex % 20);
        lineColorValue[segmentIndex] = colorStart.getValue(patternIndex);
        fadeValue[segmentIndex] = fadeStart;
        // global ("segment governance") updates
        if (segmentX > width) {
          segmentX = width - segmentX;
        }
        segmentIndex = (segmentIndex + 1 ) % segmentCount;
        sineTheta = nextTheta;
        compTheta1 = nextC1;
        compTheta2 = nextC2;
      }
    }
  }

  void plengthMessage(int plength) {
    patternLength = plength;
  }

  void gainMessage(int eValue) {
    if (eValue !=0) {
      gainActive = true;
      fadeStart = pow(float(abs(127 - eValue)) / 127.0, 5);
      //println("encoder value: "+eValue+" fade start: "+fadeStart);
    } else {
      gainActive = false;
    }
  }

  void valuesMessage(int eValue) {
    sineXVelocity.update(eValue);
    sineYVelocity.update(eValue);
    sineLengthDelta.update(eValue);
    sineAngleDelta.update(eValue);
    sineWeightStart.update(eValue);
    sineWeightDelta.update(eValue);
    sineColorDelta.update(eValue);
    compXVelocity.update(eValue);
    compYVelocity.update(eValue);
    compLengthDelta.update(eValue);
    compAngleDelta.update(eValue);
    compWeightStart.update(eValue);
    compWeightDelta.update(eValue);
    compColorDelta.update(eValue);
    lineXVelocity.update(eValue);
    lineYVelocity.update(eValue);
    lineX2Delta.update(eValue);
    lineY2Delta.update(eValue);
    lineWeightStart.update(eValue);
    lineWeightDelta.update(eValue);
    lineColorDelta.update(eValue);
    fadeDelta.update(eValue);
    colorIndex1 = int(random(min(colorPalette1.length, colorPalette2.length)));
    colorIndex2 = int(random(min(colorPalette1.length, colorPalette2.length)));
    sineAmp.update(eValue); 
    compAmp1.update(eValue); 
    compAmp2.update(eValue); 
    lineAmp.update(eValue);
    sineDelta.update(eValue); 
    compDelta1.update(eValue);
    compDelta2.update(eValue);
    segmentXDelta.update(eValue);
    colorStart.update(eValue);
    segments.update(eValue);
  }

  void initialize() {
    gainActive = true;
  }

  void shutdown() {
    for (int i = 0; i < maxSegmentCount; i++) {
      active[i] = false;
    }
  }

  void update() {
    if (gainActive) {
      for (int i = 0; i < segmentCount; i++) {
        if (active[i]) {
          int patternIndex = i % 20;
          fadeValue[i] += fadeDelta.getValue(patternIndex);
          if (fadeValue[i] >= 1) {
            active[i] = false;
          } else {
            sineX1[i] += sineXVelocity.getValue(patternIndex);
            sineY1[i] += sineYVelocity.getValue(patternIndex);
            sineLength[i] += sineLengthDelta.getValue(patternIndex);
            sineAngle[i] -= sineAngleDelta.getValue(patternIndex);
            sineWeight[i] = max(sineWeight[i] - sineWeightDelta.getValue(patternIndex), 0.5);
            sineColorValue[i] += sineColorDelta.getValue(patternIndex);
            compX1[i] += compXVelocity.getValue(patternIndex);
            compY1[i] += compYVelocity.getValue(patternIndex);
            compLength[i] += compLengthDelta.getValue(patternIndex);
            compAngle[i] -= compAngleDelta.getValue(patternIndex);
            compWeight[i] = max(compWeight[i] - compWeightDelta.getValue(patternIndex), 0.5);
            compColorValue[i] += compColorDelta.getValue(patternIndex);
            lineX1[i] += lineXVelocity.getValue(patternIndex);
            lineY1[i] += lineYVelocity.getValue(patternIndex);
            lineX2[i] = lineX2[i] + lineX2Delta.getValue(patternIndex) + lineXVelocity.getValue(patternIndex);
            lineY2[i] = lineY2[i] + lineY2Delta.getValue(patternIndex) + lineYVelocity.getValue(patternIndex);
            lineWeight[i] = max(lineWeight[i] - lineWeightDelta.getValue(patternIndex), 0.5);
            lineColorValue[i] += lineColorDelta.getValue(patternIndex);
          }
        }
      }
    }
  }

  void display() {
    if (gainActive) {
      noStroke();
      for (int i = 0; i < segmentCount; i++) {
        if (active[i]) {
          // interpolating *within* color palettes for sines
          fill(lerpColor(lerpColor(colorPalette1[colorIndex1], colorPalette1[colorIndex2], abs(sin(sineColorValue[i]))), fadeColor, fadeValue[i]));
          //strokeWeight(sineWeight[i]);
          pushMatrix();
          translate(sineX1[i], sineY1[i]);
          rotate(sineAngle[i]);
          if (sineLength[i] != 0) {
            if (i % 2 == 0) {
              triangle(0, sineWeight[i], 0, 0 - sineWeight[i], sineLength[i], 0);
            } else {
              triangle(0, 0, sineLength[i], sineWeight[i], sineLength[i], 0 - sineWeight[i]);
            }
            //line(0, 0, sineLength[i], 0);
          }
          popMatrix();
          // interpolating within color palettes for sinusoidal composites
          fill(lerpColor(lerpColor(colorPalette1[colorIndex1], colorPalette1[colorIndex2], abs(sin(compColorValue[i]))), fadeColor, fadeValue[i]));
          pushMatrix();
          translate(compX1[i], compY1[i]);
          rotate(compAngle[i]);
          if (compLength[i] != 0) {
            if (i % 2 == 0) {
              triangle(0, compWeight[i], 0, 0 - compWeight[i], compLength[i], 0);
            } else {
              triangle(0, 0, compLength[i], compWeight[i], compLength[i], 0 - compWeight[i]);
            }
          }    
          popMatrix();
          // interpolating *between* color palettes for lines
          fill(lerpColor(lerpColor(colorPalette1[colorIndex1], colorPalette2[colorIndex2], abs(sin(lineColorValue[i]))), fadeColor, fadeValue[i]));
          triangle(lineX1[i], lineY1[i] + lineWeight[i], lineX1[i], lineY1[i] - lineWeight[i], lineX2[i], lineY2[i]);
        }
      }
    }
  }
}
