# interferometry_animation

Code sample rendering the animated portion of an interactive multimedia performance created by Christopher Burns in 2019.

This repository includes only the animation code, written using the Java flavor of [Processing](https://processing.org); interactive behavior and audio generation are handled in a companion application, written in [Pure Data](http://msp.ucsd.edu/software.html). Texts originally used in performances of *Interferometry* are by multiple authors, with various copyright statuses, and have not been included in this repository.

To run this code, open interferometry_animation.pde in Processing.app. In the absence of text and OpenSoundControl messages indicating interactive performance, the animation will only generate a blank screen. Add text to the stringArray array in TextRender.pde for some very basic text animation; send OpenSoundControl messages based on the vocabulary parsed in oscEvent() (in interferometry_animation.pde) for more elaborate visual performance.

To view an example of a complete performance, please see the [sample performance documentation](https://vimeo.com/340520251).
