class Lattice {
  boolean active, isMask, isDrone;
  float currentX, currentY, fadeConstrain;
  float angle, slope, interiorFade, outlineFade, interiorColor, outlineColor;
  color[] outlinePalette, interiorPalette;
  color fadeColor;
  int currentIndex;
  Pattern xOrigin, yOrigin, xVelocity, yVelocity, latticeDimension, spacing, slopeStart, slopeDelta, interiorSpacing, angularVelocity, fillSide, latticeOrientation, outlineStrokeWeight, interiorStroke;
  Pattern outlineColorDelta, interiorColorDelta, outlineFadeDelta, interiorFadeDelta, interiorColorIndex1, interiorColorIndex2, outlineColorIndex1, outlineColorIndex2;

  Lattice(color[] cp, color fade, float isMax, float slopeMax, boolean mask, boolean drone) {
    isMask = mask;
    isDrone = drone;
    if (isMask) {
      outlinePalette = cp;
    } else {
      outlinePalette = new color[cp.length];
      for (int i = 0; i < cp.length; i++) {
        outlinePalette[i] = color(red(cp[i]), green(cp[i]), blue(cp[i]), alpha(cp[i]) * 0.66);
      }
    }
    interiorPalette = reverse(cp);
    if (isMask) {
      fadeConstrain = 0.999;
    } else {
      fadeConstrain = 1.0;
    }
    fadeColor = fade;
    active = false;
    //xOrigin = random(width * 0.1, width * 0.9);
    //yOrigin = random(height * 0.1, height * 0.9);
    angle = random(TWO_PI);
    xOrigin = new Pattern(width * 0.2, width * 0.8, 0);
    yOrigin = new Pattern(height * 0.2, height * 0.8, 0);
    xVelocity = new Pattern(-1, 1, 0);
    yVelocity = new Pattern(-1, 1, 0);
    if (isMask) {
      latticeDimension = new Pattern(height * 1.5, height * 3, 0);
    } else {
      latticeDimension = new Pattern(height, height * 2.5, 0);
    }
    spacing = new Pattern(height * 0.05, height * 0.33, 0);
    slopeStart = new Pattern(0, slopeMax, 0);
    slopeDelta = new Pattern(-0.03, 0.03, 0);
    interiorSpacing = new Pattern(height * 0.01, isMax, 0);
    angularVelocity = new Pattern(-0.002, 0.002, 0);
    if (isMask) {
      fillSide = new Pattern(0, 2.3, 0);
    } else {
      fillSide = new Pattern(0, 2.99, 0);
    }
    latticeOrientation = new Pattern(0, 1.99, 0);
    outlineStrokeWeight = new Pattern(1, 4, 0);
    interiorStroke = new Pattern(0, 1.99, 0);
    outlineColorDelta = new Pattern(0.02, 0.12, 0);
    interiorColorDelta = new Pattern(0.02, 0.12, 0);
    outlineFadeDelta = new Pattern(0.002, 0.04, 0);
    interiorFadeDelta = new Pattern(0.002, 0.04, 0);
    interiorColorIndex1 = new Pattern(0, interiorPalette.length - 0.01, 0);
    interiorColorIndex2 = new Pattern(0, interiorPalette.length - 0.01, 0);
    outlineColorIndex1 = new Pattern(0, outlinePalette.length - 0.01, 0);
    outlineColorIndex2 = new Pattern(0, outlinePalette.length - 0.01, 0);
  }

  void shutdown() {
    active = false;
  }

  void patternMessage(int index) {
    currentIndex = index;
    if (!isDrone) {
      active = true;
      currentX = xOrigin.getValue(index);
      currentY = yOrigin.getValue(index);
      interiorFade = 0;
      outlineFade = 0;
    }
    slope = slopeStart.getValue(index);
  }

  void plengthMessage(int pLength) {
  }

  void valuesMessage(int eValue) {
    xOrigin.update(eValue);
    yOrigin.update(eValue);
    if (isDrone) {
      currentX = xOrigin.getValue(currentIndex);
      currentY = yOrigin.getValue(currentIndex);
    } else {
      xVelocity.update(eValue);
      yVelocity.update(eValue);
    }
    latticeDimension.update(eValue);
    spacing.update(eValue);
    slopeStart.update(eValue);
    slopeDelta.update(eValue);
    interiorSpacing.update(eValue);
    angularVelocity.update(eValue);
    fillSide.update(eValue);
    latticeOrientation.update(eValue);
    outlineStrokeWeight.update(eValue);
    interiorStroke.update(eValue);
    outlineColorDelta.update(eValue);
    interiorColorDelta.update(eValue);
    outlineFadeDelta.update(eValue);
    interiorFadeDelta.update(eValue);
    interiorColorIndex1.update(eValue);
    interiorColorIndex2.update(eValue);
    outlineColorIndex1.update(eValue);
    outlineColorIndex2.update(eValue);
  }

  void gainMessage(int eValue) {
    if (isDrone) {
      active = !(eValue == 0);
    }
    // could potentially also control start point for interiorFade and outlineFade from here?
  }

  void update() {
    if (active) {
      slope -= slopeDelta.getValue(currentIndex); // opposite tendency...
      interiorColor += interiorColorDelta.getValue(currentIndex);
      outlineColor += outlineColorDelta.getValue(currentIndex);
      if (!isDrone) {
        angle += angularVelocity.getValue(currentIndex);
        currentX -= xVelocity.getValue(currentIndex);
        currentY -= yVelocity.getValue(currentIndex);
        interiorFade = constrain(interiorFade + interiorFadeDelta.getValue(currentIndex), 0, fadeConstrain);
        outlineFade = constrain(outlineFade + outlineFadeDelta.getValue(currentIndex), 0, fadeConstrain);
        if (interiorFade >= 1 && outlineFade >= 1) {
          active = false;
        }
      }
    }
  }

  void display() {
    if (active) {
      pushMatrix();
      translate(currentX, currentY);
      rotate(angle);
      float dimension = latticeDimension.getValue(currentIndex);
      translate(dimension * -0.5, dimension * -0.5);
      color outline = lerpColor(outlinePalette[int(outlineColorIndex1.getValue(currentIndex))], outlinePalette[int(outlineColorIndex2.getValue(currentIndex))], map(sin(outlineColor), -1, 1, 0, 1));
      color interior = lerpColor(interiorPalette[int(interiorColorIndex1.getValue(currentIndex))], interiorPalette[int(interiorColorIndex2.getValue(currentIndex))], map(cos(interiorColor), -1, 1, 0, 1));
      color finalOutline = lerpColor(outline, fadeColor, outlineFade);
      color finalInterior = lerpColor(interior, fadeColor, interiorFade);
      // lattice first
      if (interiorStroke.getValue(currentIndex) < 1) {
        fill(finalInterior);
        noStroke();
      } else {
        noFill();
        stroke(finalInterior);
        strokeWeight(int(interiorStroke.getValue(currentIndex) * 4));
      }
      float spaceValue = spacing.getValue(currentIndex);
      if (latticeOrientation.getValue(currentIndex) < 1) {
        while (spaceValue < dimension) {
          float slopeResult = spaceValue * slope;
          triangle(0, spaceValue, dimension, slopeResult, dimension, slopeResult + interiorSpacing.getValue(currentIndex));
          spaceValue += spacing.getValue(currentIndex);
        }
      } else {
        while (spaceValue < dimension) {
          float slopeResult = spaceValue * slope;
          triangle(spaceValue, 0, slopeResult, dimension, slopeResult + interiorSpacing.getValue(currentIndex), dimension);
          spaceValue += spacing.getValue(currentIndex);
        }
      }
      // followed by outlining (large) triangles
      strokeWeight(outlineStrokeWeight.getValue(currentIndex));
      switch(int(fillSide.getValue(currentIndex))) {
      case 0: // one side filled-in
        noStroke();
        fill(finalOutline);
        triangle(0, 0, dimension, 0, 0, dimension);
        noFill();
        stroke(finalOutline);
        triangle(dimension, 0, 0, dimension, dimension, dimension);
        break;
      case 1: // the other side filled-in
        noFill();
        stroke(finalOutline);
        triangle(0, 0, dimension, 0, 0, dimension);
        noStroke();
        fill(finalOutline);
        triangle(dimension, 0, 0, dimension, dimension, dimension);
        break;
      default: // neither side filled-in
        noFill();
        stroke(finalOutline);
        triangle(0, 0, dimension, 0, 0, dimension);
        triangle(dimension, 0, 0, dimension, dimension, dimension);
      }
      popMatrix();
    }
  }
}
