class Reflections extends Mode {
  Reflection[] patternReflection;
  float timerStart, timerEnd;
  int currentElement;

  Reflections(color[] a1, color[] a2, color[] a3, color fc, float weightMax, float spacingMax, int behavior) {
    super(3);
    patternReflection = new Reflection[3]; // one object (containing 20 intersections) per encoder row
    patternReflection[0] = new Reflection(a1, fc, weightMax, spacingMax, behavior);
    patternReflection[1] = new Reflection(a2, fc, weightMax, spacingMax, behavior);
    patternReflection[2] = new Reflection(a3, fc, weightMax, spacingMax, behavior);
  }

  void initialize() {
    super.initialize();
  }

  void shutdown() {
    super.shutdown();
    for (int i = 0; i < patternReflection.length; i++) {
      patternReflection[i].shutdown();
    }
  }

  void patternMessage(int mode, int row, int plength, int index, float duration) {
    currentElement = index;
    timerStart = millis();
    timerEnd = timerStart + duration;
    if (row >= 1 && row <= 3) {
      patternReflection[row - 1].patternMessage(index);
    }
  }

  void encoderMessage(int eNumber, int eValue) {
    int row = 0;
    if (eNumber >= 8 && eNumber <= 11) {
      row = 1;
    } else if (eNumber >= 12 && eNumber <= 15) {
      row = 2;
    }
    switch(eNumber) {
    case 5:
    case 9:
    case 13:
      patternReflection[row].valuesMessage(eValue);
      break;
    case 6:
    case 10:
    case 14:
      patternReflection[row].plengthMessage(int(eValue / 6.6) + 1);
      break;
    default: // ignore other encoders
    }
  }

  void update() {
    for (int i = 0; i < patternReflection.length; i++) {
      patternReflection[i].update();
    }
  }

  void display() {
    for (int i = 0; i < patternReflection.length; i++) {
      patternReflection[i].display();
    }
  }
}
