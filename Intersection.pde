class Intersection {
  int elementCount = 20;
  int currentPatternLength = 0;
  int angleCount;
  boolean[] active;
  float[] centerX, centerY, colorValue, fadeValue;
  float[][] angle;
  Pattern startX, startY, velocityX, velocityY, colorDelta, fadeDelta, colorIndex1, colorIndex2;
  Pattern[] startAngle, angleWidth, velocityAngle;
  color fadeColor;
  color[] baseColorArray;
  float sweepRadius = sqrt(pow(width, 2) + pow(height, 2));

  Intersection(int aC, color[] cA, color fC, float aW) {
    angleCount = aC;
    baseColorArray = cA;
    fadeColor = fC;
    active = new boolean[elementCount];
    centerX = new float[elementCount];
    centerY = new float[elementCount];
    colorValue = new float[elementCount];
    fadeValue = new float[elementCount];
    angle = new float[angleCount][elementCount];
    for (int i = 0; i < elementCount; i++) {
      active[i] = false;
      centerX[i] = 0;
      centerY[i] = 0;
      colorValue[i] = 0;
      fadeValue[i] = 0;
      for (int j = 0; j < angleCount; j++) {
        angle[j][i] = 0;
      }
    }
    startX = new Pattern(width * 0.2, width * 0.8, 0);
    startY = new Pattern(height * 0.2, height * 0.8, 0);
    velocityX = new Pattern(-3, 3, 0);
    velocityY = new Pattern(-3, 3, 0);
    colorDelta = new Pattern(0.1, 0.5, 0);
    fadeDelta = new Pattern(0.015, 0.05, 0); // max delta was 0.035
    colorIndex1 = new Pattern(0, baseColorArray.length - 0.01, 0);
    colorIndex2 = new Pattern(0, baseColorArray.length - 0.01, 0);
    startAngle = new Pattern[angleCount];
    angleWidth = new Pattern[angleCount];
    velocityAngle = new Pattern[angleCount];
    for (int i = 0; i < angleCount; i++) {
      startAngle[i] = new Pattern(0, TWO_PI, 0);
      angleWidth[i] = new Pattern(aW * 0.25, aW, 0);
      velocityAngle[i] = new Pattern(-0.012, 0.012, 0);
    }
  }

  void patternMessage(int index) {
    active[index] = true;
    centerX[index] = startX.getValue(index);
    centerY[index] = startY.getValue(index);
    colorValue[index] = 0;
    fadeValue[index] = 0;
    for (int i = 0; i < angleCount; i++) {
      angle[i][index] = startAngle[i].getValue(index);
    }
  }

  void plengthMessage(int pLength) {
    currentPatternLength = pLength;
  }

  void valuesMessage(int eValue) {
    startX.update(eValue);
    startY.update(eValue);
    velocityX.update(eValue);
    velocityY.update(eValue);
    colorDelta.update(eValue);
    fadeDelta.update(eValue);
    colorIndex1.update(eValue);
    colorIndex2.update(eValue);
    for (int i = 0; i < angleCount; i++) {
      startAngle[i].update(eValue);
      angleWidth[i].update(eValue);
      velocityAngle[i].update(eValue);
    }
  }

  void update() {
    for (int i = 0; i < elementCount; i++) { // was currentPatternLength, but we want unused elements to expire
      if (active[i]) {
        // inverting velocity tendency relative to position tendency
        centerX[i] -= velocityX.getValue(i); 
        centerY[i] -= velocityY.getValue(i);
        for (int j = 0; j < angleCount; j++) {
          angle[j][i] += velocityAngle[j].getValue(i);
        }
        colorValue[i] = colorValue[i] + colorDelta.getValue(i);
        fadeValue[i] = constrain(fadeValue[i] + fadeDelta.getValue(i), 0, 1);
        if (fadeValue[i] == 1) {
          active[i] = false;
        }
      }
    }
  }

  void display(int index) {
    if (active[index]) {
      float lerpValue = map(sin(colorValue[index]), -1, 1, 0, 1);
      color fillColor = lerpColor(baseColorArray[int(colorIndex1.getValue(index))], baseColorArray[int(colorIndex2.getValue(index))], lerpValue);
      fill(lerpColor(fillColor, fadeColor, fadeValue[index]));
      noStroke();
      for (int i = 0; i < angleCount; i++) {
        float cosValue1 = sweepRadius * cos(angle[i][index]);
        float endPointX1 = centerX[index] + cosValue1;
        float endPointX3 = centerX[index] - cosValue1;
        float currentWidth = angleWidth[i].getValue(index);
        float cosValue2 = sweepRadius * cos(angle[i][index] + currentWidth);
        float endPointX2 = centerX[index] + cosValue2;
        float endPointX4 = centerX[index] - cosValue2;    
        float sinValue1 = sweepRadius * sin(angle[i][index]);
        float endPointY1 = centerY[index] - sinValue1;
        float endPointY3 = centerY[index] + sinValue1;        
        float sinValue2 = sweepRadius * sin(angle[i][index] + currentWidth);
        float endPointY2 = centerY[index] - sinValue2;
        float endPointY4 = centerY[index] + sinValue2;    
        triangle(centerX[index], centerY[index], endPointX1, endPointY1, endPointX2, endPointY2);
        triangle(centerX[index], centerY[index], endPointX3, endPointY3, endPointX4, endPointY4);       
      }
    }
  }

  void shutdown() {
    for (int i = 0; i < elementCount; i++) {
      active[i] = false;
    }
  }
}
