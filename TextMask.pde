// class for "secondary text": interleaved rendering about social justice 

class TextMask {
  BufferedReader reader;
  char[][] displayText;
  boolean[][] active, rising;
  float[][] alpha;
  float[] yOrigins, xOffsets, yOffsets;
  boolean[] direction;
  String[] currentText;
  String[] compositeText = { };
  Pattern alphaRise, alphaDecay, xVelocity, yVelocity, grayDecay;
  int currentX, currentY, xDimension, yDimension, wordIndex, stringIndex, patternIndex, topEdge;
  int patternLength = 20;
  int framesElapsed = 0;
  int rateLimit = 3;
  float xMargin, flashEndpoint;
  boolean fxActive, flashActive;
  Controller fxResponse;
  float baseGray = 0;
  float flashGray = 0;
  float currentGray = 0;

  TextMask(String filename, int initialY, int textDimension) {
    reader = createReader(filename);
    if (width > 1025) {
      xDimension = 44;
    } else {
      xDimension = 35;
    }
    yDimension = 6; // must be < 20 for usage with xVelocity Pattern
    currentY = min(initialY, yDimension);
    displayText = new char[xDimension][yDimension];
    active = new boolean[xDimension][yDimension];
    rising = new boolean[xDimension][yDimension];
    alpha = new float[xDimension][yDimension];
    yOrigins = new float[yDimension];
    xOffsets = new float[yDimension];
    yOffsets = new float[yDimension];
    direction = new boolean[yDimension];
    for (int i = 0; i < xDimension; i++) {
      for (int j = 0; j < yDimension; j++) {
        active[i][j] = false;
        rising[i][j] = false;
        alpha[i][j] = 1;
      }
    }
    xMargin = float(width / xDimension + 1);
    float heightValue = float(height / (yDimension + 2));
    for (int i = 0; i < yDimension; i++) {
      yOrigins[i] = (i + 1) * heightValue;
      xOffsets[i] = 0;
      yOffsets[i] = yOrigins[i];
    }
    this.loadComposite();
    alphaRise = new Pattern(1, 2, 0);
    alphaDecay = new Pattern(1, 1.5, 0);
    grayDecay = new Pattern(3, 12, 0);
    xVelocity = new Pattern(-1.75, 1.75, 0);
    yVelocity = new Pattern(-1.75, 1.75, 0);
    fxActive = false;
    fxResponse = new Controller(0);
    topEdge = textDimension * -1;
  }

  void loadComposite() {
    String line = null;
    boolean textAvailable = true;
    while (textAvailable) {
      try {
        if ((line = reader.readLine()) != null) {
          currentText = split(line, ' ');
          for (int i = 0; i < currentText.length; i++) {
            currentText[i] = currentText[i] + " ";
          }
          compositeText = concat(compositeText, currentText);
        } else {
          textAvailable = false;
        }
      } 
      catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  void plengthMessage(int pLength) {
    patternLength = pLength;
  }

  void patternMessage(int index) {
    if (framesElapsed >= rateLimit) {
      // on any incoming pattern message, activate next character in displayText array 
      if (stringIndex >= compositeText[wordIndex].length()) { // check for end of word
        stringIndex = 0;
        wordIndex++;
      }
      if (wordIndex >= compositeText.length) { // check for end of current word array
        wordIndex = 0;
      }
      if (compositeText[wordIndex].length() - stringIndex >= xDimension - (currentX + 1)) { // check if next word > right margin
        currentX = 0;
        currentY = (currentY + 1) % yDimension;
        direction[currentY] = random(10) > 5;
        xOffsets[currentY] = 0;
      }
      displayText[currentX][currentY] = compositeText[wordIndex].charAt(stringIndex);
      stringIndex++;
      active[currentX][currentY] = true;
      rising[currentX][currentY] = true;
      alpha[currentX][currentY] = 1;
      currentX = (currentX + 1) % xDimension;
      if (currentX == 0) {
        currentY = (currentY + 1) % yDimension;
        direction[currentY] = random(10) > 5;
      }
      framesElapsed = 0;
    }
    patternIndex = index;
  }

  void fxPatternMessage(int mode, int fxID, int patternIndex, float duration) {
    if (fxActive) {
      if (fxID == 4 || fxID == 9) { 
        if (fxResponse.getValue(patternIndex)) {
          flashActive = true;
          currentGray = flashGray;
          flashEndpoint = millis() + (duration * 4);
        }
      }
    }
  }

  void encoderMessage(int number, int value) {
    switch(number) {
    case 2:
      fxActive = value >= 104; // true when at least four of five FX modules are activated
      flashGray = value * 2;
      break;
    case 3:
      fxResponse.update(value);
      grayDecay.update(value);
      break;
    case 5:
      alphaRise.update(value);
      alphaDecay.update(value);
      break;
    case 9:
      xVelocity.update(value);
      break;
    case 13:
      yVelocity.update(value);
      break;
    default:
    }
  }
  
  void modeChange() {
    baseGray = min(baseGray + 0.33, 255);
  }

  void update() {
    for (int j = 0; j < yDimension; j++) {
      for (int i = 0; i < xDimension; i++) {
        if (active[i][j]) {
          int index = (j * xDimension) + i;
          if (rising[i][j]) {
            alpha[i][j] = constrain(alpha[i][j] + alphaRise.getValue(index % patternLength), 1, 220);
            if (alpha[i][j] >= 220) {
              rising[i][j] = false;
            }
          } else {
            alpha[i][j] = constrain(alpha[i][j] - alphaDecay.getValue(index % patternLength), 1, 220);
            if (alpha[i][j] <= 1) {
              active[i][j] = false;
            }
          }
        }
      }
      if (direction[j]) {
        xOffsets[j] += xVelocity.getValue(j);
      } else {
        yOffsets[j] += yVelocity.getValue(j);
        if (yOffsets[j] < topEdge || yOffsets[j] > height) {
          yOffsets[j] = yOrigins[j];
        }
      }
    }
    if (flashActive) {
      float currentTime = millis();
      if (currentTime > flashEndpoint) {
        flashActive = false;
        currentGray = baseGray;
      } else {
        currentGray = max(0, currentGray - grayDecay.getValue(patternIndex));
      }
    }
  }

  void display() {
    textFont(futura64);
    textAlign(LEFT, TOP);
    noStroke();
    for (int j = 0; j < yDimension; j++) {
      float currentX = xMargin + xOffsets[j];
      for (int i = 0; i < xDimension; i++) {
        if (active[i][j]) {
          fill(currentGray, alpha[i][j]);
          text(displayText[i][j], currentX, yOffsets[j]); // was yOrigins[j] instead of yOffsets[j]
        }
        currentX = currentX + textWidth(displayText[i][j]);
      }
    }
    framesElapsed++;
  }
}
