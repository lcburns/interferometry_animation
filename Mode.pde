class Mode {
  boolean active = false;
  int launchCount, modeIndex;
  
  Mode(int mi) {
    modeIndex = mi;
  }
  
  void initialize() {
    launchCount++;
    active = true;
  }
  
  void shutdown() {
    active = false;
  }
  
  void update() { 
  }
  
  void display() {
  }
  
  void patternMessage(int patternMode, int patternRow, int patternLength, int patternIndex, float patternDuration) {
  }
  
  void encoderMessage(int encoderNumber, int encoderValue) {
  }
  
  void report() {
  }
}
