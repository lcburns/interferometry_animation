class Meshes extends Mode {
  Mesh[] patternMesh;
  float timerStart, timerEnd;
  int currentElement;

  Meshes(color[] a1, color[] a2, color[] a3, color fc, float fadeMin, float fadeMax, float minHeight) {
    super(3);
    patternMesh = new Mesh[3]; // one object (containing 20 intersections) per encoder row
    patternMesh[0] = new Mesh(a1, fc, fadeMin, fadeMax, minHeight, height * 0.25);
    patternMesh[1] = new Mesh(a2, fc, fadeMin, fadeMax, minHeight, height * 0.75);
    patternMesh[2] = new Mesh(a3, fc, fadeMin, fadeMax, minHeight, height * 0.5);      
  }
  
  void initialize() {
    super.initialize();
  }

  void shutdown() {
    super.shutdown();
    for (int i = 0; i < patternMesh.length; i++) {
      patternMesh[i].shutdown();
    }
  }

  void patternMessage(int mode, int row, int plength, int index, float duration) {
    currentElement = index;
    timerStart = millis();
    timerEnd = timerStart + duration;
    if (row >= 1 && row <= 3) {
      patternMesh[row - 1].patternMessage(index);
    }
  }

  void encoderMessage(int eNumber, int eValue) {
    int row = 0;
    if (eNumber >= 8 && eNumber <= 11) {
      row = 1;
    } else if (eNumber >= 12 && eNumber <= 15) {
      row = 2;
    }
    switch(eNumber) {
    case 5:
    case 9:
    case 13:
      patternMesh[row].valuesMessage(eValue);
      break;
    case 6:
    case 10:
    case 14:
      patternMesh[row].plengthMessage(int(eValue / 6.6) + 1);
      break;
    default: // ignore other encoders
    }
  }

  void update() {
    for (int i = 0; i < patternMesh.length; i++) {
      patternMesh[i].update();
    }
  }

  void display() {
    for(int i = 0; i < patternMesh.length; i++) {
      patternMesh[i].display();
    }
  }
}
