class Reflection {
  int elementCount = 20;
  int maxSegmentCount = 8;
  int currentPatternLength = 0;
  int animationMode;
  boolean[] active;
  int[] animationTypeValue;
  float[] colorValue, fadeValue, rotationValue;
  float[][] xPoints, yPoints, riseValue;
  Pattern origin, weight, segmentCount, colorDelta, fadeDelta, colorIndex1, colorIndex2, angle, angleVel, axis, animationType;
  Pattern[] riseStart, riseVelocity; 
  color fadeColor;
  color[] baseColorArray;

  // animationMode: 0 = basic horizontal / vertical, 1 = variable spacing, 2 = variable motion, 3 = rotated, 4 = rotated + variable spacing
  //   5 = rotated + variable motion, 6 = angularMotion, 7 = angularMotion + variable spacing, 8 = angular motion + variable motion
  // interactionMode: 0 = trigger new figure on every /pattern

  Reflection(color[] cA, color fC, float weightMax, float spacingMax, int aM) {
    baseColorArray = cA;
    fadeColor = fC;
    animationMode = aM; // sets global vertical/horizontal/rotating behavior
    active = new boolean[elementCount];
    animationTypeValue = new int[elementCount];
    colorValue = new float[elementCount];
    fadeValue = new float[elementCount];
    rotationValue = new float[elementCount];
    xPoints = new float[elementCount][maxSegmentCount + 1];
    yPoints = new float[elementCount][maxSegmentCount + 1];
    riseValue = new float[elementCount][maxSegmentCount];
    origin = new Pattern(0.1, 0.9, 0);
    weight = new Pattern(weightMax * 0.2, weightMax, 0);
    segmentCount = new Pattern(2, maxSegmentCount, 0);
    colorDelta = new Pattern(0.2, 0.8, 0);
    fadeDelta = new Pattern(0.01, 0.04, 0); // fade max was 0.03
    colorIndex1 = new Pattern(0, baseColorArray.length - 0.01, 0);
    colorIndex2 = new Pattern(0, baseColorArray.length - 0.01, 0);
    angle = new Pattern(0, TWO_PI, 0);
    angleVel = new Pattern(-0.01, 0.01, 0);
    axis = new Pattern(0, 1.99, 0);
    animationType = new Pattern(max(0, animationMode - 4.0), animationMode + 0.99, 0); // governs rotating/motion behavior of specific element
    riseStart = new Pattern[maxSegmentCount];
    riseVelocity = new Pattern[maxSegmentCount];
    for (int i = 0; i < maxSegmentCount; i++) {
      riseStart[i] = new Pattern(spacingMax * -1, spacingMax, 0);
      riseVelocity[i] = new Pattern(-2, 2, 0);
    }
  }

  void patternMessage(int index) {
    active[index] = true;
    //animationTypeValue[index] = int(animationType.getValue(index)) % (animationMode + 1); // is this the desirable way to handle this?
    animationTypeValue[index] = int(animationType.getValue(index));
    colorValue[index] = 0;
    fadeValue[index] = 0;
    switch(animationTypeValue[index]) {
    case 1: 
    case 2: 
    case 4: 
    case 5: 
    case 7: 
    case 8: // types with variable spacing
      for (int i = 0; i < maxSegmentCount; i++) {
        riseValue[index][i] = riseStart[i].getValue(index); // riseStart.length == maxSegmentCount
      }
      break;
    default:
      for (int i = 0; i < maxSegmentCount; i++) {
        riseValue[index][i] = riseStart[0].getValue(index); // riseStart.length == maxSegmentCount
      }
    }
    rotationValue[index] = angle.getValue(index); // unused in animationMode = 0, 1, 2
    if (int(axis.getValue(index)) < 1 || animationTypeValue[index] >= 3) { // horizontal orientation 
      xPoints[index][0] = 0;
      yPoints[index][0] = origin.getValue(index) * height;
    } else { // vertical orientation
      xPoints[index][0] = origin.getValue(index) * width;
      yPoints[index][0] = 0;
    }
    tracePoints(index);
  }

  void tracePoints(int index) {
    if (animationTypeValue[index] >= 3) { // rotating motion = larger shape, horizontal orientation only
      for (int i = 1; i <= segmentCount.getValue(index); i++) {
        float extension = width * 1.5;
        float negExtension = width * -0.5;
        if (i % 2 == 0) {
          xPoints[index][i] = extension;
          yPoints[index][i] = yPoints[index][i - 1] - riseValue[index][i - 1];
        } else {
          xPoints[index][i] = negExtension;
          yPoints[index][i] = yPoints[index][i - 1] - riseValue[index][i - 1];
        }
      }
    } else if (int(axis.getValue(index)) < 1) { // horizontal orientation, no rotation
      for (int i = 1; i <= segmentCount.getValue(index); i++) {
        if (i % 2 == 0) {
          xPoints[index][i] = 0;
          yPoints[index][i] = yPoints[index][i - 1] - riseValue[index][i - 1];
        } else {
          xPoints[index][i] = width;
          yPoints[index][i] = yPoints[index][i - 1] - riseValue[index][i - 1];
        }
      }
    } else { // vertical orientation, no rotation
      for (int i = 1; i <= segmentCount.getValue(index); i++) {
        if (i % 2 == 0) {
          xPoints[index][i] = xPoints[index][i - 1] - riseValue[index][i - 1];
          yPoints[index][i] = 0;
        } else {
          xPoints[index][i] = xPoints[index][i - 1] - riseValue[index][i - 1];
          yPoints[index][i] = height;
        }
      }
    }
  }

  void plengthMessage(int pLength) {
    currentPatternLength = pLength;
  }

  void valuesMessage(int eValue) {
    origin.update(eValue);
    weight.update(eValue);
    segmentCount.update(eValue);
    colorDelta.update(eValue);
    fadeDelta.update(eValue);
    colorIndex1.update(eValue);
    colorIndex2.update(eValue);
    animationType.update(eValue);
    angle.update(eValue);
    angleVel.update(eValue);
    axis.update(eValue);
    for (int i = 0; i < maxSegmentCount; i++) {
      riseStart[i].update(eValue);
      riseVelocity[i].update(eValue);
    }
  }

  // animationMode: 0 = basic horizontal / vertical, 1 = variable spacing, 2 = variable motion, 3 = rotated, 4 = rotated + variable spacing
  //   5 = rotated + variable motion, 6 = angularMotion, 7 = angularMotion + variable spacing, 8 = angular motion + variable motion

  void update() {
    for (int i = 0; i < elementCount; i++) { // was currentPatternLength, but we want unused elements to expire
      if (active[i]) {
        if (animationTypeValue[i] >= 6) { // rotating animationTypes
          rotationValue[i] += angleVel.getValue(i);
        }
        colorValue[i] = colorValue[i] + colorDelta.getValue(i);
        fadeValue[i] = constrain(fadeValue[i] + fadeDelta.getValue(i), 0, 1);
        if (fadeValue[i] == 1) {
          active[i] = false;
        }
        if (animationTypeValue[i] == 2 || animationTypeValue[i] == 5 || animationTypeValue[i] == 8) { // types with variable motion
          for (int j = 0; j < maxSegmentCount; j++) {
            riseValue[i][j] += riseVelocity[j].getValue(i); // variable velocity applied to spacing
          }
        } else {
          for (int j = 0; j < maxSegmentCount; j++) {
            riseValue[i][j] += riseVelocity[0].getValue(i); // fixed velocity applied to (constant or variable) spacing
          }
        }
        tracePoints(i);
      }
    }
  }

  void display() {
    for (int i = 0; i < elementCount; i++) {
      if (active[i]) {
        boolean isRotating = false;
        if (animationTypeValue[i] >= 6) { // rotating animation types
          pushMatrix();
          translate(width * 0.5, height * 0.5);
          rotate(rotationValue[i]);
          translate(width * -0.5, height * -0.5);
          isRotating = true;
        }
        float lerpValue = map(sin(colorValue[i]), -1, 1, 0, 1);
        color strokeColor = lerpColor(baseColorArray[int(colorIndex1.getValue(i))], baseColorArray[int(colorIndex2.getValue(i))], lerpValue);
        stroke(lerpColor(strokeColor, fadeColor, fadeValue[i]));
        strokeWeight(weight.getValue(i));
        noFill();
        for (int j = 1; j <= segmentCount.getValue(i); j++) {
          line(xPoints[i][j - 1], yPoints[i][j - 1], xPoints[i][j], yPoints[i][j]);
        }
        if (isRotating) {
          popMatrix();
        }
      }
    }
  }

  void shutdown() {
    for (int i = 0; i < elementCount; i++) {
      active[i] = false;
    }
  }
}
