class Lattices extends Mode {
  Lattice[] patternLattice;
  Reflection[] patternReflection;
  float timerStart, timerEnd;
  int currentElement;
  boolean isMask;

  Lattices(color[] a1, color[] a2, color[] a3, color fc, float isMax, float slopeMax, boolean mask, boolean drone) {
    super(3);
    patternLattice = new Lattice[3]; // one object (containing 20 lattices) per encoder row
    patternLattice[0] = new Lattice(a1, fc, isMax, slopeMax, mask, drone);
    patternLattice[1] = new Lattice(a2, fc, isMax, slopeMax, mask, drone);
    patternLattice[2] = new Lattice(a3, fc, isMax, slopeMax, mask, drone);
    isMask = mask;
    if (isMask) {
      patternReflection = new Reflection[3]; // plus one object (containing 20 reflections) per encoder row
      patternReflection[0] = new Reflection(a1, color(0, 1), 30, 10, 8);
      patternReflection[1] = new Reflection(a2, color(0, 1), 30, 10, 8);
      patternReflection[2] = new Reflection(a3, color(0, 1), 30, 10, 8);
    }
  }

  void initialize() {
    super.initialize();
  }

  void shutdown() {
    super.shutdown();
    for (int i = 0; i < patternLattice.length; i++) {
      patternLattice[i].shutdown();
    }
    if (isMask) {
      for (int i = 0; i < patternReflection.length; i++) {
        patternReflection[i].shutdown();
      }
    }
  }

  void patternMessage(int mode, int row, int plength, int index, float duration) {
    currentElement = index;
    timerStart = millis();
    timerEnd = timerStart + duration;
    if (row >= 1 && row <= 3) {
      patternLattice[row - 1].patternMessage(index);
      if (isMask) {
        patternReflection[row - 1].patternMessage(index);
      }
    }
  }

  void encoderMessage(int eNumber, int eValue) {
    int row = 0;
    if (eNumber >= 8 && eNumber <= 11) {
      row = 1;
    } else if (eNumber >= 12 && eNumber <= 15) {
      row = 2;
    }
    switch(eNumber) {
    case 4:
    case 8:
    case 12:
      patternLattice[row].gainMessage(eValue);  
      break;
    case 5:
    case 9:
    case 13:
      patternLattice[row].valuesMessage(eValue);
      if (isMask) {
        patternReflection[row].valuesMessage(eValue);
      }
      break;
    case 6:
    case 10:
    case 14:
      patternLattice[row].plengthMessage(int(eValue / 6.6) + 1);
      if (isMask) {
        patternReflection[row].valuesMessage(eValue);
      }
      break;
    default: // ignore other encoders
    }
  }

  void update() {
    for (int i = 0; i < patternLattice.length; i++) {
      patternLattice[i].update();
    }
    if (isMask) {
      for (int i = 0; i < patternReflection.length; i++) {
        patternReflection[i].update();
      }
    }
  }

  void display() {
    for (int i = 0; i < patternLattice.length; i++) {
      patternLattice[i].display();
    }
    if (isMask) {
      for (int i = 0; i < patternReflection.length; i++) {
        patternReflection[i].display();
      }
    }
  }
}
