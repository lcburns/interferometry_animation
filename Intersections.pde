class Intersections extends Mode {
  Intersection[] patternIntersection;
  float timerStart, timerEnd;
  int currentElement;

  Intersections(int ac, color[] a1, color[] a2, color[] a3, color fc, float angleValue) {
    super(0);
    patternIntersection = new Intersection[3]; // one object (containing 20 intersections) per encoder row
    patternIntersection[0] = new Intersection(ac, a1, fc, angleValue);
    patternIntersection[1] = new Intersection(ac, a2, fc, angleValue);
    patternIntersection[2] = new Intersection(ac, a3, fc, angleValue);
  }
  
  void initialize() {
    super.initialize();
  }

  void shutdown() {
    super.shutdown();
    for (int i = 0; i < patternIntersection.length; i++) {
      patternIntersection[i].shutdown();
    }
  }

  void patternMessage(int mode, int row, int plength, int index, float duration) {
    currentElement = index;
    timerStart = millis();
    timerEnd = timerStart + duration;
    if (row >= 1 && row <= 3) {
      patternIntersection[row - 1].patternMessage(index);
    }
  }

  void encoderMessage(int eNumber, int eValue) {
    int row = 0;
    if (eNumber >= 8 && eNumber <= 11) {
      row = 1;
    } else if (eNumber >= 12 && eNumber <= 15) {
      row = 2;
    }
    switch(eNumber) {
    case 5:
    case 9:
    case 13:
      patternIntersection[row].valuesMessage(eValue);
      break;
    case 6:
    case 10:
    case 14:
      patternIntersection[row].plengthMessage(int(eValue / 6.6) + 1);
      break;
    default: // ignore other encoders
    }
  }

  void update() {
    for (int i = 0; i < patternIntersection.length; i++) {
      patternIntersection[i].update();
    }
  }

  void display() {
    for (int i = 0; i < 20; i++) {
      for (int j = 0; j < patternIntersection.length; j++) {
        patternIntersection[j].display(i);
      }
    }
  }
}
