class Traces extends Mode {
  Trace[] patternTrace;

  Traces(color[] c1, color[] c2, color[] c3, color[] c4, color[] c5, color fc, float fadeMin, float fadeMax, boolean global, float wMin, float wMax, float vel) {
    super(1);
      patternTrace = new Trace[3]; 
      patternTrace[0] = new Trace(144, height * 0.45, fadeMin, fadeMax, height * 0.75, c1, c4, fc, global, wMin, wMax, vel);
      patternTrace[1] = new Trace(216, height * 0.5, fadeMin, fadeMax, height * 0.75, c2, c4, fc, global, wMin, wMax, vel);
      patternTrace[2] = new Trace(180, height * 0.55, fadeMin, fadeMax, height * 0.75, c3, c4, fc, global, wMin, wMax, vel);
  }

  void initialize() {
    super.initialize();
    for (int i = 0; i < patternTrace.length; i++) {
      patternTrace[i].initialize();
    }
  }

  void shutdown() {
    super.shutdown();
    for (int i = 0; i < patternTrace.length; i++) {
      patternTrace[i].shutdown();
    }
  }

  void patternMessage(int mode, int row, int plength, int index, float duration) {
    if (row >= 1 && row <= 3) {
      patternTrace[row - 1].patternMessage(index);
    }
  }

  void encoderMessage(int eNumber, int eValue) {
    int row = 0;
    if (eNumber >= 8 && eNumber <= 11) {
      row = 1;
    } else if (eNumber >= 12 && eNumber <= 15) {
      row = 2;
    }
    switch(eNumber) {
    case 4:
    case 8:
    case 12:
      patternTrace[row].gainMessage(eValue);
      break;
    case 5:
    case 9:
    case 13:
      patternTrace[row].valuesMessage(eValue);
      break;
    case 6:
    case 10:
    case 14:
      patternTrace[row].plengthMessage(int(eValue / 6.6) + 1);
      break;
    default: // ignore other encoders
    }
  }

  void update() {
    for (int i = 0; i < patternTrace.length; i++) {
      patternTrace[i].update();
    }
  }

  void display() {
    for (int i = 0; i < patternTrace.length; i++) {
      patternTrace[i].display();
    }
  }
}
