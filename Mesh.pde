class Mesh {
  int totalElements = 20;
  int patternLength = totalElements;
  float baseline;
  boolean[] active;
  float[] fadeTheta, rotation, xOffset, yOffset;
  Pattern fadeDelta, colorIndex1, colorIndex2, yPoint, yPoint2, yPoint3, xExtension, rotationDelta, xVelocity, yVelocity; 
  Controller orientation;
  color[] baseColorArray;
  color fadeColor;

  Mesh(color[] bc, color fc, float fadeMin, float fadeMax, float minHeight, float bl) {
    baseColorArray = bc;
    fadeColor = fc;
    active = new boolean[totalElements];
    fadeTheta = new float[totalElements];
    rotation = new float[totalElements];
    xOffset = new float[totalElements];
    yOffset = new float[totalElements];
    fadeDelta = new Pattern(fadeMin, fadeMax, 0);
    colorIndex1 = new Pattern(0, baseColorArray.length - 0.01, 0);
    colorIndex2 = new Pattern(0, baseColorArray.length - 0.01, 0);
    yPoint = new Pattern(min(minHeight, height * 0.5), height * 0.75, 0);
    yPoint2 = new Pattern(min(minHeight, height * 0.5), height * 0.75, 0);
    yPoint3 = new Pattern(min(minHeight, height * 0.01), height * 0.85, 0); // wa was height * 0.25
    xExtension = new Pattern(0, 100, 0);
    rotationDelta = new Pattern(PI * -0.002, PI * 0.002, 0);
    xVelocity = new Pattern(-2, 2, 0);
    yVelocity = new Pattern(-2, 2, 0);
    orientation = new Controller(0);
    for (int i = 0; i < totalElements; i++) {
      fadeTheta[i] = 0;
    }
    baseline = bl;
  }

  void patternMessage(int index) {
    fadeTheta[index] = 0;
    rotation[index] = 0;
    xOffset[index] = 0;
    yOffset[index] = 0;
    active[index] = true;
  }

  void plengthMessage(int pLength) {
    patternLength = pLength;
  }

  void valuesMessage(int eValue) {
    fadeDelta.update(eValue);
    colorIndex1.update(eValue);
    colorIndex2.update(eValue);
    yPoint.update(eValue);
    yPoint2.update(eValue);
    yPoint3.update(eValue);
    xExtension.update(eValue);
    rotationDelta.update(eValue);
    xVelocity.update(eValue);
    yVelocity.update(eValue);
    orientation.update(eValue);
  }

  void update() {
    for (int i = 0; i < totalElements; i++) {
      fadeTheta[i] += constrain(fadeDelta.getValue(i), 0, 1);
      //if (fadeTheta[i] >= 1) {
      //  active[i] = false;
      //}
      if (patternLength > 3) {
        rotation[i] += rotationDelta.getValue(i);
        xOffset[i] += xVelocity.getValue(i);
        yOffset[i] += yVelocity.getValue(i);
      }
      if(xOffset[i] < - 101 || xOffset[i] > width + 100) {
        active[i] = false;
      }
    }
  }

  void display() {
    noStroke();
    float spacing = float(width) / float(patternLength);
    for (int index = 0; index < patternLength; index++) {
      if (active[index]) {
        color baseColor1 = baseColorArray[int(colorIndex1.getValue(index))];
        color baseColor2 = baseColorArray[int(colorIndex2.getValue(index))];
        color fadeValue1 = lerpColor(baseColor1, fadeColor, fadeTheta[index]);
        color fadeValue2 = lerpColor(baseColor2, fadeColor, fadeTheta[index]);
        float x0 = xOffset[index] + spacing * index;
        float y0 = yOffset[index] + baseline;
        pushMatrix();
        translate(x0, y0);
        rotate(rotation[index]);  
        float y1 = yPoint.getValue(index);
        float y2 = yPoint2.getValue(index);
        float y3 = yPoint3.getValue(index);
        float x1 = spacing + xExtension.getValue(index);
        if (orientation.getValue(index)) {
          fill(fadeValue1);
          quad(0, y1, 0, y1 * -1, x1, y3 * -1, x1, y3);
          fill(fadeValue2);
          quad(0, y2, 0, y2 * -1, x1, y3 * -1, x1, y3);
        } else {
          fill(fadeValue1);
          quad(x1, y1, x1, y1 * -1, 0, y3 * -1, 0, y3);
          fill(fadeValue2);
          quad(x1, y2, x1, y2 * -1, 0, y3 * -1, 0, y3);
        }
        popMatrix();
      }
    }
  }

  void shutdown() {
  }
}
