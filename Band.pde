class Band {
  int elementCount = 20;
  int nestedElements = 7;
  int currentPatternLength = 0;
  boolean[] active;
  float[] xOrigin, dimensionValue, marginValue, fadeValue;
  float[][] colorValue;
  Pattern xStart, xVelocity, dimensionStart, dimensionDelta, marginStart, marginDelta, fadeDelta, nestedActive;
  Pattern[] colorDelta, colorIndex;
  color fadeColor;
  color[] baseColorArray;

  Band(color[] bca, color fc, float dim, float alphaScale) {
    //baseColorArray = bca;
    baseColorArray = new color[bca.length];
    arrayCopy(bca, baseColorArray);
    fadeColor = fc;
    active = new boolean[elementCount];
    xOrigin = new float[elementCount];
    dimensionValue = new float[elementCount];
    marginValue = new float[elementCount];
    fadeValue = new float[elementCount];
    colorValue = new float[elementCount][nestedElements];
    xStart = new Pattern(width * 0.1, width * 0.9 - dim, 0);
    xVelocity = new Pattern(-2, 2, 0);
    dimensionStart = new Pattern(dim * 0.2, dim, 0);
    dimensionDelta = new Pattern(-1, 1, 0);
    marginStart = new Pattern(dim * -0.25, dim * 0.25, 0);
    marginDelta = new Pattern(-0.5, 0.5, 0);
    fadeDelta = new Pattern(0.005, 0.025, 0); // max value was 0.015
    nestedActive = new Pattern(2, nestedElements + 0.99, 0);
    colorDelta = new Pattern[nestedElements];
    colorIndex = new Pattern[nestedElements];
    for (int i = 0; i < nestedElements; i++) {
      colorDelta[i] = new Pattern(0.05, 0.2, 0);
      colorIndex[i] = new Pattern(0, baseColorArray.length - 0.01, 0);
    }
    for (int i = 0; i < baseColorArray.length; i++) {
      // winch down alpha values significantly for paletttes shared with other drawing behaviors
      baseColorArray[i] = color(red(baseColorArray[i]), green(baseColorArray[i]), blue(baseColorArray[i]), alpha(baseColorArray[i]) * alphaScale);
    }
  }

  void patternMessage(int index) {
    xOrigin[index] = xStart.getValue(index);
    dimensionValue[index] = dimensionStart.getValue(index);
    marginValue[index] = marginStart.getValue(index);
    for (int i = 0; i < nestedElements; i++) {
      colorValue[index][i] = 0;
      colorValue[index][i] = 0;
      colorValue[index][i] = 0;
    }
    fadeValue[index] = 0;
    active[index] = true;
  }

  void plengthMessage(int pLength) {
    currentPatternLength = pLength;
  }

  void valuesMessage(int eValue) {
    nestedActive.update(eValue);
    xStart.update(eValue);
    xVelocity.update(eValue);
    dimensionStart.update(eValue);
    dimensionDelta.update(eValue);
    marginStart.update(eValue);
    marginDelta.update(eValue);
    fadeDelta.update(eValue);
    for (int i = 0; i < nestedElements; i++) {
      colorDelta[i].update(eValue);
      colorIndex[i].update(eValue);
    }
  }

  void update() {
    for (int i = 0; i < elementCount; i++) {
      if (active[i]) {
        xOrigin[i] -= xVelocity.getValue(i);
        dimensionValue[i] -= dimensionDelta.getValue(i);
        marginValue[i] -= marginDelta.getValue(i);
        fadeValue[i] += fadeDelta.getValue(i);
        for (int j = 0; j < nestedElements; j++) {     
          colorValue[i][j] += colorDelta[j].getValue(i);
          colorValue[i][j] += colorDelta[j].getValue(i);
          colorValue[i][j] += colorDelta[j].getValue(i);
        }
        if (fadeValue[i] >= 1) {
          active[i] = false;
        }
      }
    }
  }

  void display() {
    noStroke();
    rectMode(CORNER);
    for (int i = 0; i < elementCount; i++) {
      if (active[i]) {
        for (int j = 0; j < int(nestedActive.getValue(i)); j++) {
          float lerpValue = map(sin(colorValue[i][j]), -1, 1, 0, 1);
          color colorMix = lerpColor(baseColorArray[int(colorIndex[j].getValue(i))], baseColorArray[int(colorIndex[(j + 1) % 3].getValue(i))], lerpValue);
          color fadeCalc = lerpColor(colorMix, fadeColor, fadeValue[i]);
          fill(fadeCalc);
          float marginOffset = j * marginValue[i];
          rect(xOrigin[i] - (marginOffset * 0.5), 0, dimensionValue[i] + marginOffset, height);
        }
      }
    }
  }

  void shutdown() {
    for (int i = 0; i < elementCount; i++) {
      active[i] = false;
    }
  }
}
