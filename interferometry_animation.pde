// 2022.09.20 version for sharing with github
// textfiles (which have various authors / copyrights) removed
// animation portion only; audio / interactive control code is separate (written in Pure Data)
import oscP5.*; // library supporting OpenSoundControl network messaging

OscP5 oscP5base;
FXBehavior fxLayer;
Mode[] modeArray, maskArray;
TextRender foregroundText;
TextMask[] occlusionText;
Pattern occlusionPattern;
String[] textFiles = {  };  // textfiles removed

int modeIndex, maskIndex;
PFont futura64; // actually Gill Sans, but the variable name is what it is
color[] palette1a = { color(40, 40, 200, 220), color(60, 20, 200, 220), color(70, 10, 200, 220), color(20, 60, 180, 220), color(255, 220) };
color[] palette1b = { color(30, 30, 200, 220), color(50, 10, 200, 220), color(70, 10, 180, 220), color(10, 50, 200, 220), color(255, 220) };
color[] palette1c = { color(20, 20, 200, 220), color(30, 10, 200, 220), color(50, 10, 180, 220), color(10, 30, 200, 220), color(240, 220) };
//color[] palette1d = { color(240, 240, 0, 220), color(245, 235, 0, 220), color(250, 230, 0, 220), color(230, 250, 220), color(255, 220) };
color[] palette1d = { color(70, 70, 140, 220), color(120, 0, 160, 220), color(50, 50, 180, 220), color(80, 0, 200, 220), color(255, 220) };
color[] palette1e = { color(255, 220), color(70, 70, 140, 220), color(80, 80, 120, 220), color(80, 100, 100, 220), color(100, 100, 110, 220) };
color[] palette2a = { color(40, 40, 240, 220), color(40, 80, 200, 220), color(40, 120, 160, 220), color(40, 140, 140, 220),
  color(40, 40, 240, 220), color(80, 40, 200, 220), color(120, 40, 160, 220), color(140, 40, 140, 220) };
color[] palette2b = { color(40, 40, 240, 220), color(40, 80, 200, 220), color(40, 120, 160, 220), color(40, 140, 140, 220),
  color(20, 160, 140, 220), color(20, 200, 100, 220), color(20, 120, 180, 220), color(20, 80, 220, 220) }; // identical to palette1
color[] palette2c = { color(40, 40, 240, 220), color(40, 80, 200, 220), color(40, 120, 160, 220), color(40, 140, 140, 220),
  color(20, 150, 150, 220), color(20, 160, 140, 220), color(20, 180, 120, 220), color(20, 200, 100, 220) };
float la = 105; // used for mode 6 at present (was 75)
color[] palette2ala = { color(40, 40, 240, la), color(40, 80, 200, la), color(40, 120, 160, la), color(40, 140, 140, la),
  color(40, 40, 240, 220), color(80, 40, 200, la), color(120, 40, 160, la), color(140, 40, 140, la) };
color[] palette2bla = { color(40, 40, 240, la), color(40, 80, 200, la), color(40, 120, 160, la), color(40, 140, 140, la),
  color(20, 160, 140, 220), color(20, 200, 100, la), color(20, 120, 180, la), color(20, 80, 220, la) }; // identical to palette1
color[] palette2cla = { color(40, 40, 240, la), color(40, 80, 200, la), color(40, 120, 160, la), color(40, 140, 140, la),
  color(20, 150, 150, 220), color(20, 160, 140, la), color(20, 180, 120, la), color(20, 200, 100, la) };
color[] palette3a = { color(180, 40, 100, 220), color(160, 40, 120, 220), color(120, 40, 160, 220), color(140, 40, 140, 220),
  color(180, 100, 40, 220), color(200, 120, 0, 220), color(190, 130, 0, 220), color(220, 60, 0, 220) };
color[] palette3b = { color(180, 40, 100, 220), color(170, 40, 110, 220), color(160, 40, 120, 220), color(150, 40, 130, 220),
  color(180, 100, 40, 220), color(200, 100, 20, 220), color(170, 110, 20, 220), color(190, 90, 20, 220) };
color[] palette3c = { color(180, 40, 100, 220), color(190, 50, 80, 220), color(200, 60, 60, 220), color(210, 70, 40, 220),
  color(220, 80, 20, 220), color(230, 90, 0, 220), color(240, 40, 40, 220), color(220, 50, 50, 220) };
color[] palette3aw = { color(180, 40, 100, 220), color(160, 40, 120, 220), color(120, 40, 160, 220), color(140, 40, 140, 220),
  color(180, 100, 40, 220), color(200, 120, 0, 220), color(190, 130, 0, 220), color(220, 60, 0, 220), color(255, 180),
  color(180, 40, 100, 220), color(160, 40, 120, 220), color(120, 40, 160, 220), color(140, 40, 140, 220),
  color(180, 100, 40, 220), color(200, 120, 0, 220), color(190, 130, 0, 220), color(220, 60, 0, 220) };
color[] palette3bw = { color(180, 40, 100, 220), color(170, 40, 110, 220), color(160, 40, 120, 220), color(150, 40, 130, 220),
  color(180, 100, 40, 220), color(200, 100, 20, 220), color(170, 110, 20, 220), color(190, 90, 20, 220), color(255, 180),
  color(180, 40, 100, 220), color(170, 40, 110, 220), color(160, 40, 120, 220), color(150, 40, 130, 220),
  color(180, 100, 40, 220), color(200, 100, 20, 220), color(170, 110, 20, 220), color(190, 90, 20, 220) };
color[] palette3cw = { color(180, 40, 100, 220), color(190, 50, 80, 220), color(200, 60, 60, 220), color(210, 70, 40, 220),
  color(220, 80, 20, 220), color(230, 90, 0, 220), color(240, 40, 40, 220), color(220, 50, 50, 220), color(255, 180),
  color(180, 40, 100, 220), color(190, 50, 80, 220), color(200, 60, 60, 220), color(210, 70, 40, 220),
  color(220, 80, 20, 220), color(230, 90, 0, 220), color(240, 40, 40, 220), color(220, 50, 50, 220) };
color[] palette4a = { color(140, 0, 140, 220), color(160, 0, 150, 220), color(180, 0, 160, 220), color(200, 0, 170, 220), color(255, 220) };
color[] palette4b = { color(150, 0, 150, 220), color(175, 0, 160, 220), color(200, 0, 170, 220), color(225, 0, 180, 220), color(255, 220) };
color[] palette4c = { color(160, 0, 160, 220), color(190, 0, 170, 220), color(220, 0, 180, 220), color(250, 0, 190, 220), color(255, 220) };
color[] fadePalette1 = { color(0, 170), color(0, 220) };
color[] fadePalette2 = { color(0, 120), color(0, 220) };
color[] fadePalette3 = { color(0, 70), color(0, 220) };
color[] fadePalette4 = { color(0, 5), color(0, 10), color(0, 20), color(0, 40), color(0, 80), color(0, 40), color(0, 20), color(0, 10), color(0, 5) };
color[] fadePalette5 = { color(0, 240), color(0, 120), color(0, 60), color(0, 30), color(0, 60), color(0, 120), color(0, 240), color(0, 220), color(0, 200) };
color[] fadePalette6 = { color(0, 5), color(0, 10), color(0, 20), color(0, 40), color(0, 80), color(0, 160), color(0, 80), color(0, 40), color(0, 20), color(0, 10), color(0, 5) };
color[] palette14a = concat(concat(fadePalette1, palette3a), fadePalette1);
color[] palette14b = concat(concat(fadePalette2, palette3b), fadePalette2);
color[] palette14c = concat(concat(fadePalette3, palette3c), fadePalette3);

void setup() {
  size(1280, 720, P2D); // 720p version; could run at higher resolutions on modern hardware
  pixelDensity(displayDensity());
  oscP5base = new OscP5(this, 3003);
  fxLayer = new FXBehavior(concat(palette1a, palette1b), concat(palette2a, palette2b), concat(palette3a, palette3b), concat(palette4a, palette4b));
  modeArray   = new Mode[16];
  maskArray = new Mode[16];
  modeArray[0] = new Intersections(1, palette1a, palette1b, palette1c, color(0, 1), 0.15);
  maskArray[0] = new Intersections(1, fadePalette1, fadePalette2, fadePalette3, color(0), 0.25);
  modeArray[1] = new Intersections(3, palette2a, palette2b, palette2c, color(0, 1), 0.1); // values for old mode 2 now attached to mode 1, mode 1 color palette
  maskArray[1] = new Intersections(3, fadePalette1, fadePalette2, fadePalette3, color(0), 0.4);
  modeArray[2] = new Lattices(palette3aw, palette3bw, palette3cw, color(0), height * 0.15, 7.5, false, false); // newly created mode 2 2019.03.21
  maskArray[2] = new Lattices(fadePalette4, fadePalette5, fadePalette5, color(0), height * 0.15, 5, true, false);
  modeArray[3] = new Intersections(1, palette4a, palette4b, palette4c, color(0, 1), 0.15);
  maskArray[3] = new Reflections(fadePalette1, fadePalette2, fadePalette3, color(0), 25, 150, 8);
  modeArray[4] = new Reflections(palette1a, palette1b, palette1c, color(0, 1), 15, 10, 0);
  maskArray[4] = new Reflections(fadePalette1, fadePalette2, fadePalette3, color(0, 1), 30, 10, 1);
  modeArray[5] = new Reflections(palette2ala, palette2bla, palette2cla, color(0, 1), 15, 150, 3);
  maskArray[5] = new Reflections(fadePalette1, fadePalette2, fadePalette3, color(0, 1), 30, 150, 4);
  modeArray[6] = new Reflections(palette3a, palette3b, palette3c, color(0, 1), 15, 20, 7);
  maskArray[6] = new Reflections(fadePalette1, fadePalette2, fadePalette3, color(0, 1), 30, 45, 5);
  modeArray[7] = new Reflections(palette4a, palette4b, palette4c, color(0, 1), 15, 150, 8);
  maskArray[7] = new Intersections(3, fadePalette1, fadePalette1, fadePalette1, color(0, 1), 0.4);
  modeArray[8] = new Meshes(palette1a, palette1b, palette1c, color(0, 1), 0.005, 0.03, height * 0.1);
  maskArray[8] = new Meshes(fadePalette4, fadePalette4, fadePalette4, color(0), 0.001, 0.04, height * 0.5);
  modeArray[9] = new Traces(palette2a, palette2b, palette2c, palette2ala, palette2cla, color(0, 1), 0.0018, 0.012, false, 1, 12, 1.25);
  maskArray[9] = new Bands(fadePalette1, fadePalette2, fadePalette3, color(0), 20, 0.33);
  modeArray[10] = new Meshes(palette3a, palette3b, palette3c, color(0, 1), 0.005, 0.03, height * 0.1);
  maskArray[10] = new Lattices(fadePalette4, fadePalette5, fadePalette5, color(0), height * 0.05, 10, true, false);
  modeArray[11] = new Lattices(palette4a, palette4b, palette4c, color(0, 1), height * 0.15, 5, false, true);
  maskArray[11] = new Lattices(fadePalette4, fadePalette5, fadePalette5, color(0), height * 0.15, 5, true, false);
  modeArray[12] = new Traces(palette1a, palette1b, palette1c, palette1d, palette1e, color(0, 1), 0.0018, 0.006, true, 1, 12, 2.25);
  maskArray[12] = new Lattices(fadePalette4, fadePalette5, fadePalette5, color(0), height * 0.15, 5, true, false);
  modeArray[13] = new Bands(palette2a, palette2b, palette2c, color(0, 1), 90, 0.25);
  maskArray[13] = new Reflections(fadePalette1, fadePalette2, fadePalette3, color(0, 16), 45, 75, 8);
  modeArray[14] = new Bands(palette14a, palette14b, palette14c, color(0, 1), 45, 1);
  maskArray[14] = new Bands(fadePalette1, fadePalette2, fadePalette3, color(0, 1), 90, 0.1);
  modeArray[15] = new Bands(concat(fadePalette1, palette4a), concat(fadePalette2, palette4b), concat(palette4c, fadePalette3), color(0), 90, 0.4);
  maskArray[15] = new Meshes(fadePalette6, fadePalette6, fadePalette6, color(0, 1), 0.001, 0.04, height * 0.5);
  modeIndex = 0;
  maskIndex = 0;
  int maskTextDimension;
  if (width <= 1281) {
    futura64 = createFont("GillSans", 64);
    maskTextDimension = 64;
  } else {
    futura64 = createFont("GillSans", 96);
    maskTextDimension = 96;
  }
  foregroundText = new TextRender(7500, palette2a, palette3a); // was 7500 display interval
  foregroundText.initialize();
  occlusionText = new TextMask[textFiles.length];
  for (int i = 0; i < textFiles.length; i++) {
    occlusionText[i] = new TextMask(textFiles[i], (i * 2) % 6, maskTextDimension);
  }
  occlusionPattern = new Pattern(0, textFiles.length -0.01, 0);
}

void draw() {
  fxLayer.update();
  fxLayer.display();
  modeArray[modeIndex].update();
  modeArray[modeIndex].display();
  for (int i = 0; i < occlusionText.length; i++) {
    occlusionText[i].update();
    occlusionText[i].display();
  }
  maskArray[maskIndex].update();
  maskArray[maskIndex].display();
  foregroundText.update();
  foregroundText.display();
}

void oscEvent(OscMessage incomingMessage) {
  // parse for /mode, /pattern, /encoder, /outputgain
  if (incomingMessage.checkAddrPattern("/mode")) {
    if (incomingMessage.checkTypetag("f")) {
      int incomingMode = int(incomingMessage.get(0).floatValue()) - 1;
      modeIndex = incomingMode;
      maskIndex = incomingMode;
      modeArray[modeIndex].initialize();
      maskArray[maskIndex].initialize();
      int textChoice = int(random(occlusionText.length)); // advancing one mask text per pattern event
      occlusionText[textChoice].modeChange();
      //println("mode switch: "+incomingMode+" mode index: "+modeIndex+" mask index: "+maskIndex);
    }
  } else if (incomingMessage.checkAddrPattern("/pattern")) {
    if (incomingMessage.checkTypetag("fffff")) {
      int patternMode = int(incomingMessage.get(0).floatValue());
      int patternRow = int(incomingMessage.get(1).floatValue());
      int patternLength = int(incomingMessage.get(2).floatValue());
      int patternIndex = int(incomingMessage.get(3).floatValue());
      float patternDuration = incomingMessage.get(4).floatValue();
      //println("pattern row: "+patternRow+" pattern index: "+patternIndex);
      modeArray[modeIndex].patternMessage(patternMode, patternRow, patternLength, patternIndex, patternDuration);
      maskArray[maskIndex].patternMessage(patternMode, patternRow, patternLength, patternIndex, patternDuration);
      foregroundText.patternMessage(patternIndex);
      int textChoice = int(occlusionPattern.getValue(patternIndex)); // advancing one mask text per pattern event
      occlusionText[textChoice].patternMessage(patternIndex);
      //for(int i = 0; i < occlusionText.length; i++) {
      //  occlusionText[i].patternMessage(patternIndex); // advancing all mask texts instead of one at a time
      //}
    }
  } else if (incomingMessage.checkAddrPattern("/encoder")) {
    if (incomingMessage.checkTypetag("ff")) {
      int encoderNumber = int(incomingMessage.get(0).floatValue());
      int encoderValue = int(incomingMessage.get(1).floatValue());
      //println("encoder number: "+encoderNumber+" value: "+encoderValue);
      fxLayer.encoderMessage(encoderNumber, encoderValue);
      modeArray[modeIndex].encoderMessage(encoderNumber, encoderValue);
      maskArray[maskIndex].encoderMessage(encoderNumber, encoderValue);
      foregroundText.encoderMessage(encoderNumber, encoderValue);
      for (int i = 0; i < occlusionText.length; i++) {
        occlusionText[i].encoderMessage(encoderNumber, encoderValue);
      }
      if (encoderNumber == 1) {
        occlusionPattern.update(encoderValue);
      }
    }
  } else if (incomingMessage.checkAddrPattern("/fx")) {
    if (incomingMessage.checkTypetag("fff")) {
      int fxID = int(incomingMessage.get(0).floatValue());
      int patternIndex = int(incomingMessage.get(1).floatValue());
      float patternDuration = incomingMessage.get(2).floatValue();
      fxLayer.setBehavior(modeIndex, fxID, patternIndex, patternDuration);
      int textChoice = int(occlusionPattern.getValue(patternIndex));
      occlusionText[textChoice].fxPatternMessage(modeIndex, fxID, patternIndex, patternDuration);
    }
  } else if (incomingMessage.checkAddrPattern("/outputgain")) {
    // consider using outputgain == 0 as off switch/gesture for animation
    if (incomingMessage.checkTypetag("f")) {
      float gainValue = incomingMessage.get(0).floatValue();
      println("output gain setting: "+gainValue);
    }
  } else if (incomingMessage.checkAddrPattern("/state")) {
    // transition and recall messages: jumpcut, fadeout, perforate, recall, randomize
    if (incomingMessage.checkTypetag("s")) {
      String messageString = incomingMessage.get(0).stringValue();
      println("state message: "+messageString);
    }
  }
}
