class Bands extends Mode {
  Band[] patternBand;

  Bands(color[] c1, color[] c2, color[] c3, color fc, float dim, float alphaScale) {
    super(1);
    patternBand = new Band[3]; // one object (containing 20 intersections) per encoder row
    patternBand[0] = new Band(c1, fc, dim, alphaScale);
    patternBand[1] = new  Band(c2, fc, dim, alphaScale);
    patternBand[2] = new  Band(c3, fc, dim, alphaScale);
  }

  void initialize() {
    super.initialize();
  }

  void shutdown() {
    super.shutdown();
    for (int i = 0; i < patternBand.length; i++) {
      patternBand[i].shutdown();
    }
  }

  void patternMessage(int mode, int row, int plength, int index, float duration) {
    if (row >= 1 && row <= 3) {
      patternBand[row - 1].patternMessage(index);
    }
  }

  void encoderMessage(int eNumber, int eValue) {
    int row = 0;
    if (eNumber >= 8 && eNumber <= 11) {
      row = 1;
    } else if (eNumber >= 12 && eNumber <= 15) {
      row = 2;
    }
    switch(eNumber) {
    case 5:
    case 9:
    case 13:
      patternBand[row].valuesMessage(eValue);
      break;
    case 6:
    case 10:
    case 14:
      patternBand[row].plengthMessage(int(eValue / 6.6) + 1);
      break;
    default: // ignore other encoders
    }
  }

  void update() {
    for (int i = 0; i < patternBand.length; i++) {
      patternBand[i].update();
    }
  }

  void display() {
    for (int i = 0; i < patternBand.length; i++) {
      patternBand[i].display();
    }
  }
}
