class Pattern {
  int maxLength = 20;
  float[] aValues, bValues;
  Controller patternController;
  float minValue, maxValue;

  Pattern(float minV, float maxV, int init) {
    aValues = new float[maxLength];
    bValues = new float[maxLength];
    patternController = new Controller(init);
    minValue = minV;
    maxValue = maxV;
    setAValues(lowConstant()); // don't use new update() to handle init
    setBValues(lowConstant()); // because you need to fill both arrays at start
  }

  float getValue(int index) {
    float returnValue;
    if (patternController.getValue(index)) {
      returnValue = bValues[index];
    } else {
      returnValue = aValues[index];
    }
    return returnValue;
  }

  void update(int updateInput) {
    // on /encoder messages from Pd, update controller (and sometimes) value arrays
    patternController.update(updateInput);
    switch(updateInput) {
    case 0:
      setBValues(lowConstant());
      break;
    case 15:
      setAValues(lowConstant());
      break;
    case 16:
      setAValues(lowLinear());
      break;
    case 31:
      setBValues(lowPattern());
      break;
    case 32:
      setBValues(step());
      break;
    case 47:
      setAValues(lowLinear());
      break;
    case 48:
      setAValues(normalConstant());
      break;
    case 63:
      setBValues(step());
      break;
    case 64:
      setBValues(waveform());
      break;
    case 79:
      setAValues(normalConstant());
      break;
    case 80:
      setAValues(highLinear());
      break;
    case 95:
      setBValues(waveform());
      break;
    case 96:
      setBValues(highPattern());
      break;
    case 111:
      setAValues(highLinear());
      break;
    case 112:
      setAValues(highConstant());
      break;
    case 127:
      setBValues(highConstant());
      break;
    default:
      // no-op for remaining 0-127 values
    }
  }

  void setAValues(float[] inputArray) {
    arrayCopy(inputArray, aValues);
  }

  void setBValues(float[] inputArray) {
    arrayCopy(inputArray, bValues);
  }

  float[] normalConstant() {
    float[] tempArray = new float[maxLength];
    float constantValue = lerp(minValue, maxValue, random(1.0));
    for (int i = 0; i < maxLength; i++) {
      tempArray[i] = constantValue;
    }
    return tempArray;
  }

  float[] lowConstant() {
    float[] tempArray = new float[maxLength];
    float constantValue = lerp(minValue, maxValue, min(random(1.0), random(1.0)));
    for (int i = 0; i < maxLength; i++) {
      tempArray[i] = constantValue;
    }
    return tempArray;
  }

  float[] highConstant() {
    float[] tempArray = new float[maxLength];
    float constantValue = lerp(minValue, maxValue, max(random(1.0), random(1.0)));
    for (int i = 0; i < maxLength; i++) {
      tempArray[i] = constantValue;
    }
    return tempArray;
  }

  float[] lowPattern() {
    float[] tempArray = new float[maxLength];
    int patternLength = int(random(1, 5.99));
    float[] patternArray = new float[patternLength];
    for (int i = 0; i < patternArray.length; i++) {
      patternArray[i] = lerp(minValue, maxValue, min(random(1.0), random(1.0)));
    }
    for (int i = 0; i  < maxLength; i++) {
      tempArray[i] = patternArray[i % patternLength];
    }
    return tempArray;
  }

  float[] highPattern() {
    float[] tempArray = new float[maxLength];
    float[] patternArray = new float[int(random(1, 5.99))];
    for (int i = 0; i < patternArray.length; i++) {
      patternArray[i] = lerp(minValue, maxValue, max(random(1.0), random(1.0)));
    }
    for (int i = 0; i  < maxLength; i++) {
      tempArray[i] = patternArray[i % patternArray.length];
    }
    return tempArray;
  }

  float[] step() {
    float[] tempArray = new float[maxLength];
    float start = lerp(minValue, maxValue, random(1.0));
    if (random(1.0) > 0.5) {
      for (int i = 0; i < maxLength; i++) {
        tempArray[i] = lerp(start, maxValue, float(i) / float(maxLength - 1));
      }
    } else {
      for (int i = 0; i < maxLength; i++) {
        tempArray[i] = lerp(start, minValue, float(i) / float(maxLength - 1));
      }
    }
    return tempArray;
  }

  float[] lowLinear() {
    float[] tempArray = new float[maxLength];
    for (int i = 0; i < maxLength; i++) {
      tempArray[i] = lerp(minValue, maxValue, min(random(1.0), random(1.0)));
    }
    return tempArray;
  }

  float[] highLinear() {
    float[] tempArray = new float[maxLength];
    for (int i = 0; i < maxLength; i++) {
      tempArray[i] = lerp(minValue, maxValue, max(random(1.0), random(1.0)));
    }
    return tempArray;
  }

  float[] waveform() {
    float[] tempArray = new float[maxLength]; 
    float zeroPoint = random(0.5) + 0.25;
    if (random(1.0) > 0.5) {
      float amplitude = random(0.25);
      float theta = random(0.07, 1.57);
      for (int i = 0; i < maxLength; i++) {
        tempArray[i] = lerp(minValue, maxValue, (amplitude * cos(theta * i)) + zeroPoint);
      }
    } else {
      float startPoint = random(1000);
      float step = min(random(1000), random(1000));
      for (int i = 0; i < maxLength; i++) {
        tempArray[i] = map(((step * i) + startPoint) % 1000, 0, 1000, minValue, maxValue);
      }
    }
    return tempArray;
  }
}
