// class for "primary text": foreground rendering about Michelson & Morley / framing concept

class TextRender {
  //centered values:
  //float writeX[] = { width * 0.497, width * 0.503, width * 0.5 };
  //float writeY[] = { height * 0.3, height * 0.4, height * 0.5 }; // was 0.3, 0.5, 0.7
  //left-aligned values:
  float writeX[] = { width * 0.075, width * 0.075, width * 0.075 };
  float writeY[] = { height * 0.15, height * 0.25, height * 0.35 };  
  float fadeValue;
  color black, white, fade, fadewhite;
  color[] colorArray1, colorArray2;
  int colorIndex1, colorIndex2;
  Pattern colorPattern1, colorPattern2;
  String[] stringArray = { }; // text removed along with other textfiles
  float[] launchTimes = { 1, 4, 5, 6, 7, 8, 9, 67, 68, 69, 70, 71, 112, 114 };  // 15-minute version
  int[] lineCount = { 1, 1, 2, 3, 2, 3, 2, 2, 2, 3, 3, 3, 2, 2 };
  float displayStart, fadeStart, displayEnd, displayInterval, halfInterval;
  int arrayIndex = 0;
  int readPosition = 0;
  int arrayLength = min(launchTimes.length, lineCount.length);
  boolean active = false;

  TextRender(float dI, color[] ca1, color[] ca2) {
    displayInterval = dI;
    halfInterval = displayInterval * 0.5;
    colorArray1 = ca1;
    colorArray2 = ca2;
    black = color(0); 
    white = color(220, 220);
    fade = color(0, 1);
    fadewhite= color(220, 1);
    colorPattern1 = new Pattern(0, colorArray1.length - 0.01, 0);
    colorPattern2 = new Pattern(0, colorArray2.length - 0.01, 0);
  }

  void patternMessage(int index) {
    colorIndex1 = int(colorPattern1.getValue(index));
    colorIndex2 = int(colorPattern2.getValue(index));
  }

  void encoderMessage(int eNumber, int eValue) {
    switch(eNumber) {
    case 5:
      colorPattern1.update(eValue);
      break;
    case 9:
      colorPattern2.update(eValue);
      break;
    default:
    }
  }

  void initialize() {
    readPosition = 0;
    arrayIndex = 0;
    active = false;
  }

  void update() {
    if (arrayIndex < arrayLength) {
      float currentTime = millis();
      if (!active && currentTime > launchTimes[arrayIndex] * displayInterval) {
        displayStart = currentTime;
        fadeStart = displayStart + halfInterval;
        displayEnd = displayStart + displayInterval;
        active = true;
      } else if (active && currentTime > displayEnd) {
        active = false;
        readPosition = readPosition + lineCount[arrayIndex];
        arrayIndex++;
      } else if (currentTime > fadeStart) {
        fadeValue = (displayEnd - currentTime) / halfInterval;
      } else {
        fadeValue = (currentTime - displayStart) / halfInterval;
      }
    }
  }

  void display() {
    if (active) {
      noStroke();
      textFont(futura64);
      //textAlign(CENTER, CENTER);
      textAlign(LEFT, CENTER);
      for (int i = 0; i < lineCount[arrayIndex]; i++) {
        //fill(lerpColor(fade, colorArray1[colorIndex1], fadeValue));
        //text(stringArray[readPosition + i], writeX[0] + 3 * fadeValue, writeY[i]); 
        //fill(lerpColor(fade, colorArray2[colorIndex2], fadeValue));    
        //text(stringArray[readPosition + i], writeX[1] - 3 * fadeValue, writeY[i]); 
        fill(lerpColor(fadewhite, white, fadeValue));
        text(stringArray[readPosition + i], writeX[2], writeY[i]);
      }
    }
  }
}
